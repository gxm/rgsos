<?php
 // SAE 自带的 NOSQL 数据库
$kv = new SaeKV();
$mmc = new Memcached();

class SAE_DATA
{
    public $expire_time = 7200;

    public function get($default = [])
    {
        global $mmc;
        global $kv;
        $data = $mmc->get($this->key);
        if ($data === false) {
            $data = $kv->get($this->key);
            if ($data === false) {
                while (true) {
                    if ($mmc->add($this->key . '.lock', 1, 1)) {
                        $kv->set($this->key, $default);
                        $mmc->set($this->key, $default, $this->expire_time);
                        $mmc->delete($this->key . '.lock');
                        return $default;
                    }
                    usleep(random_int(15000, 30000));
                }
            } else {
                $mmc->set($this->key, $data, $this->expire_time);
            }
        }
        return $data;
    }

    public function clear()
    {
        global $mmc;
        global $kv;
        $kv->delete($this->key);
        $mmc->delete($this->key);
        $mmc->delete($this->key . '.lock');
    }
}

/* SAE_STATE
 * 服务器维护的用户状态数组
 * $kv->set 只是为了在服务器上留个底，之后也不会有主动写入到服务器的操作
 * 在用户断开连接的时候才会写入
 */
class SAE_STATE extends SAE_DATA
{
    public function __construct(string $gid, string $key, string $uid = "")
    {
        $this->key = DB . $gid . '/S-' . $key . '/' . $uid;
        $this->gid = $gid;
        $this->uid = $uid;
        $this->k = $key;
    }

    public function update(array $update)
    {
        global $mmc;
        global $kv;
        if ($this->uid !== "") {
            $data = $this->get();
            foreach ($update as $key => $value) {
                if ($value === null) {
                    unset($data[$key]);
                } else {
                    $data[$key] = $value;
                }
            }
            $mmc->set($this->key, $data, $this->expire_time);
            return [0, ['gid' => $this->gid, $this->k => [$this->uid, $update, microtime(true)]]];
        } else {
            foreach ($update as $key => $value) {
                while (true) {
                    if ($mmc->add($this->key . $key . '.lock', 1, 1)) {
                        if ($value === null) {
                            $kv->delete($this->key . $key);
                            $mmc->delete($this->key. $key);
                        } else {
                            $kv->set($this->key . $key, $value);
                            $mmc->set($this->key . $key, $value, $this->expire_time);
                        }
                        $mmc->delete($this->key . $key . '.lock');
                        break;
                    } else {
                        usleep(random_int(15000, 30000));
                    }
                }
            }
            return [0, ['gid' => $this->gid, $this->k => [$this->uid, $update, microtime(true)]]];
        }
    }

    public function dup()
    {
        global $mmc;
        global $kv;
        while (true) {
            if ($mmc->add($this->key . '.lock', 1, 1)) {
                $kv->set($this->key, $this->get());
                $mmc->delete($this->key);
                $mmc->delete($this->key . '.lock');
                break;
            } else {
                usleep(random_int(15000, 30000));
            }
        }
    }
}

/* SAE_VALUE
 * 永久存储的值
 */
class SAE_VALUE extends SAE_DATA
{
    public function __construct(string $gid, string $key)
    {
        $this->key = DB . $gid . '/V-' . $key;
        $this->k = $key;
        $this->gid = $gid;
    }

    public function update($value, string $method = 'set')
    {
        global $kv;
        global $mmc;
        while (true) {
            if ($mmc->add($this->key . '.lock', 1, 1)) {
                switch ($method) {
                    case 'push': // $value 是添加到末尾的元素
                        $data = $this->get();
                        array_push($data, $value);
                        break;
                    case 'diff': // $value 是需要被删掉的值
                        $data = $this->get();
                        $data = array_values(array_diff($data, [$value]));
                        break;
                    case 'unset': // $value 是清除的键值
                        $data = $this->get();
                        unset($data[$value]);
                        break;
                    case 'set':
                        $data = $value;
                        break;
                    default:
                        return [1, 'invalid method'];
                }
                $kv->set($this->key, $data);
                $mmc->delete($this->key);
                $mmc->delete($this->key . '.lock');
                return [0, ['gid' => $this->gid, $this->k => [$method, $value, microtime(true)]]];
            } else {
                usleep(random_int(15000, 30000));
            }
        }
    }
}
