<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://cdn.bootcss.com/github-markdown-css/2.10.0/github-markdown.css" rel="stylesheet">
    <style>
        .markdown-body {
            box-sizing: border-box;
            min-width: 480px;
            max-width: 1440px;
            margin: 0 auto;
            padding: 45px;
        }
    </style>
    <title>rgsos - all games</title>
</head>

<body>
    <article class="markdown-body">
        <?php
        if (!is_https()) {
            echo "<blockquote>For your security, this website requires HTTPS.</blockquote>";
            $url = "https://" . $_SERVER['HTTP_HOST'];
            echo "<p>Goto: <a href=\"${url}\">${url}</a></p>";
        }
        require 'base.php';
        $stor = new sinacloud\sae\Storage();
        $list = $stor->getBucket("rsa", "");
        ?>
        <h1>Create Game</h1>
        <form action="/group.php" method="GET">
            <input name="db" type="text" />
            <input name="gid" type="hidden" value="server" />
        </form>
        <h2>All Games
            <?php
            echo "(" . sizeof($list, 0) . "/" . Max_Game_Num . ")";
            ?>
        </h2>
        <ul>
            <?php
            foreach ($list as $file) {
                $game = explode("/", $file["name"])[0];
                echo "<li><a href=\"/${game}/server\">" . $game . "</a></li>";
            }
            ?>
        </ul>
        <a href="https://gitlab.com/gxm/rgsos/wikis/home">Visit Wiki</a>
    </article>
</body>

</html> 