<?php
 // --------------------------------------------------------
// 定义 3 个基本常量
// --------------------------------------------------------
if (!defined(DB)) {
    define(DB, $_GET["db"] . "/");
}
define(Bucket, "data");
// 服务器最多支持的游戏数量
define(Max_Game_Num, 5);
// --------------------------------------------------------
// 引用标准数据库
// --------------------------------------------------------
require 'sae_data.php';

// --------------------------------------------------------
// 展示数据
// --------------------------------------------------------
function group_keys(string $gid): array
{
    global $kv;
    $ret = $kv->pkrget(DB . $gid, 100);
    while (true) {
        end($ret);
        $start_key = key($ret);
        $i = count($ret);
        if ($i < 100) break;
        $ret = $kv->pkrget(DB . $gid, 100, $start_key);
    }
    return $ret;
}

// 整理数据库中的数据, 保存到各个分类里
function keys2data(string $gid, array $keys): array
{
    $data = ['value' => [], 'state' => []];
    $prefix = DB . $gid . "/";
    $n = strlen($prefix);
    foreach ($keys as $key => $value) {
        $k = substr($key, $n);
        switch ($k[0]) {
            case 'V':
                $data['value'][substr($k, 2)] = $value;
                break;
            case 'S':
                list($state, $uid) = explode("/", substr($k, 2), 2);
                if (!array_key_exists($state, $data['state'])) {
                    $data['state'][$state] = [];
                }
                $data['state'][$state][$uid] = $value;
                break;
            default:
                $data['value'][$k] = $value;
                break;
        }
    }
    return $data;
}

function clear_group(string $gid, bool $clear_files = true)
{
    $keys = group_keys($gid);
    global $kv;
    global $mmc;
    foreach ($keys as $k => $v) {
        $kv->delete($k);
        $mmc->delete($k);
    }
    if ($clear_files) {
        remove_files($gid);
    }
}
// --------------------------------------------------------
// 自动调用的方法
// --------------------------------------------------------
// 当第一个组员加入群组时
function start_group(string $gid)
{ }
// 当最后一个组员离开群组时
// 当群组名以 _ 开头时, 删掉整个群组
// 否则, 删掉全部以 _ 开头的 state, 并 dump 不以 _ 开头的 state
function end_group(string $gid)
{
    $tmp_prefix = '_';
    if (substr($gid, 0, 1) === $tmp_prefix) {
        clear_group($gid);
    } else {
        $keys = group_keys($gid);
        $data = keys2data($gid, $keys);
        foreach ($data['state'] as $key => $contents) {
            foreach ($contents as $uid => $value) {
                $k = new SAE_STATE($gid, $key, $uid);
                if (substr($key, 0, 1) === $tmp_prefix) {
                    $k->clear();
                } else {
                    $k->dup();
                }
            }
        }
    }
}

function reset_game(): array
{
    list($privKey, $pubKey) = sshKeyGen();
    // rsa 私钥保存在 stroage / rsa 里
    file_put_contents("saestor://rsa/" . DB . "id_rsa", $privKey);
    $password = bin2hex(openssl_random_pseudo_bytes(16));
    $k = new SAE_VALUE('server', '#password');
    $k->update(checksum($password));
    $k = new SAE_VALUE('u-0', '#password');
    $k->update(checksum($password));
    $game = substr(DB, 0, -1);
    return [$pubKey . ' ' . $game, $password];
}
// --------------------------------------------------------
// 文件处理相关
// --------------------------------------------------------
function k_file(string $gid, string $file_name): string
{
    return "saestor://" . Bucket . '/' . DB . $gid . '/' . $file_name;
}

function put_file(string $gid, string $file_name, string $contents)
{
    global $mmc;
    $path = k_file($gid, $file_name);
    while (true) {
        if ($mmc->add(DB . $path . '.lock', 1, 1)) {
            if ($contents) {
                file_put_contents($path, $contents);
            } else {
                unlink($path);
            }
            $mmc->delete(DB . $path . '.lock');
            return;
        } else {
            usleep(random_int(30000, 60000));
        }
    }
}

function get_file(string $gid, string $file_name): string
{
    $stor = new sinacloud\sae\Storage();
    @$response = $stor->getObject(Bucket, DB . $gid . '/' . $file_name);
    if ($response !== false) {
        return $response->body;
    } else {
        return "";
    }
}

function remove_files(string $gid)
{
    $stor = new sinacloud\sae\Storage();
    if (substr($gid, -1) != '/') {
        $gid .= '/';
    }
    $list = $stor->getBucket(Bucket, DB . $gid);
    foreach ($list as $file) {
        $stor->deleteObject(Bucket, $file['name']);
    }
}

// --------------------------------------------------------
// channel / 数据传输相关
// --------------------------------------------------------
function uid2client(string $uid): string
{
    return DB . "u-" . $uid;
}

function client2uid(string $client): string
{
    $n = strlen(DB . "u-");
    return substr($client, $n);
}

function channel_url(string $gid, string $uid, int $expire_time = 7200): string
{
    $client = uid2client($uid);
    $channel = new SaeChannel();
    return $channel->createChannel($client, $expire_time);
}

function broadcast(string $gid, string $message, array $exclude)
{
    $channel = new SaeChannel();
    $k_online = new SAE_VALUE($gid, 'online');
    $online = array_unique($k_online->get());
    $online = array_diff($online, $exclude);
    foreach ($online as $uid) {
        if (ctype_digit($uid)) {
            $client = uid2client($uid);
            $channel->sendMessage($client, $message, true);
        }
    }
}

function recvdata(string $text = ""): array
{
    if (!$text) {
        $text = trim(file_get_contents("php://input"));
    }
    switch (substr($text, 0, 2)) {
        case 'r=':
            return rsa_decode(substr($text, 2));
        case 'a=':
            return aes_decode(substr($text, 2), AES_KEY);
        default:
            return json_decode($text, true);
    }
}

// --------------------------------------------------------
// rgsos_main 事务处理的函数
// --------------------------------------------------------
function rgsos_main_ws($recv)
{
    // channel: connected / disconnected
    if ($recv['action'] === 'channel_connected') {
        $uid = $recv['cid'];
        $k_online = new SAE_VALUE("u-$uid", 'online');
        $log = $k_online->update($uid, 'push');
        return $log;
    }

    if ($recv['action'] === 'channel_disconnected') {
        $uid = $recv['cid'];
        $k_online = new SAE_VALUE("u-$uid", 'online');
        $log = $k_online->update($uid, 'diff');
        $k_groups = new SAE_VALUE("u-$uid", 'groups');
        // 退出所有的 groups
        foreach ($k_groups->get() as $_gid) {
            $_recv = ['action' => 'exit_group', 'gid' => $_gid, 'cid' => $uid];
            rgsos_main_ws($_recv);
        }
        return $log;
    }

    // group 操作
    // 将来会在这里添加 permission
    $gid = $recv['gid'];

    // 加入群组
    if ($recv['action'] === 'join_group') {
        $uid = $recv['cid'];
        // uid = 0, super user
        if ($uid != '0') {
            $k_password = new SAE_VALUE($gid, '#password');
            $password_md5 = $k_password->get();
            if ($password_md5) {
                $password = $recv['password'];
                if (checksum($password) !== $password_md5) {
                    return [1, 'wrong password'];
                }
            }
        }
        // user groups
        $k_groups = new SAE_VALUE("u-$uid", 'groups');
        $k_groups->update($gid, 'push');
        // group online
        $k_online = new SAE_VALUE($gid, 'online');
        if (empty($k_online->get())) {
            start_group($gid);
        }
        $log = $k_online->update($uid, 'push');
        return $log;
    }

    if ($recv['action'] === 'exit_group') {
        $uid = $recv['cid'];
        // u-$uid 的 groups 一定要被清空
        $k_groups = new SAE_VALUE("u-$uid", 'groups');
        $k_groups->update($gid, 'diff');
        // 从对应 group 中删除 online 并群发消息
        $k_online = new SAE_VALUE($gid, 'online');
        $log = $k_online->update($uid, 'diff');
        if (empty($k_online->get())) {
            end_group($gid);
            $log = [2, "end group"];
        }
        return $log;
    }

    if ($recv['action'] === 'get_value') {
        $key = $recv['key'];
        $k = new SAE_VALUE($gid, $key);
        return [0, ['gid' => $gid, $k->k => ['get', $k->get(), microtime(true)]]];
    }

    // 可能获取到很大的数值, 但是因为有压缩，当前情况下还可以接受    
    if ($recv['action'] === 'get_state') {
        $data = group_keys($gid . '/S-' . $recv['key']);
        $n = strlen(DB . $gid);
        $keys = array_map(function ($v) use ($n) {
            return substr($v, $n + 3);
        }, array_keys($data));
        $values = [];
        foreach ($keys as $k) {
            list($key, $uid) = explode('/', $k, 2);
            $k_state = new SAE_STATE($gid, $key, $uid);
            array_push($values, $k_state->get());
        }
        return [0, ['gid' => $gid, 'keys' => $keys, 'values' => $values, 't' => microtime(true)]];
    }

    // 以下操作需要已登录才可以继续执行
    // 修改 value / state
    // 读、写文件
    $k_online = new SAE_VALUE($gid, 'online');
    $online = array_unique($k_online->get());
    if (!in_array($recv['cid'], $online)) {
        return [3, "not in group"];
    }

    // Update Value / State
    if ($recv['action'] === 'update_value') {
        $key = $recv['key'];
        $value = $recv['value'];
        $method = $recv['method'];
        if ($key === 'password' && $method === 'set') {
            $key = '#password';
            $value = checksum($value);
        }
        if (is_array($value)) {
            if (array_key_exists('SERVER_TIME_STAMP', $value)) {
                $value['SERVER_TIME_STAMP'] = microtime(true);
            }
        }
        $k = new SAE_VALUE($gid, $key);
        $log = $k->update($value, $method);
        return $log;
    }

    if ($recv['action'] === 'update_state') {
        $uid = (string)$recv['uid'];
        $key = $recv['key'];
        $update = $recv['update'];
        $k_state = new SAE_STATE($gid, $key, $uid);
        $log = $k_state->update($update);
        return $log;
    }

    if ($recv['action'] === 'dup_state') {
        $data = group_keys($gid . '/S-' . $recv['key']);
        $n = strlen(DB . $gid);
        $keys = array_map(function ($v) use ($n) {
            return substr($v, $n + 3);
        }, array_keys($data));
        foreach ($keys as $k) {
            list($key, $uid) = explode('/', $k, 2);
            $k_state = new SAE_STATE($gid, $key, $uid);
            $k_state->dup();
        }
        return [0, ['gid' => $gid]];
    }

    return [4, "invalid action"];
}

function rgsos_main_http($recv)
{
    // 注册
    if ($recv['action'] === 'register') {
        // total users += 1
        $k = new SAE_VALUE('server', 'total_users');
        $total_users = $k->get(0);
        $total_users += 1;
        $k->update($total_users);
        // set password
        $password = bin2hex(openssl_random_pseudo_bytes(16));
        $k = new SAE_VALUE("u-$total_users", '#password');
        $k->update(checksum($password));
        $k = new SAE_VALUE("u-$total_users", 'name');
        $k->update("u-$total_users");
        return [0, ['uid' => $total_users, 'password' => $password]];
    }

    // 登录
    if ($recv['action'] === 'login') {
        $uid = $recv['uid'];
        $gid = "u-$uid";
        // 判断密码是否正确
        $k_password = new SAE_VALUE($gid, '#password');
        if ($k_password) {
            $password = $recv['password'];
            if (checksum($password) !== $k_password->get()) {
                return [1, 'wrong password'];
            }
        }
        // 已在线玩家
        $k_online = new SAE_VALUE($gid, 'online');
        if (in_array($uid, $k_online->get())) {
            $k_online->update($uid, 'diff');
            return [2, 'onlined user'];
        }
        // 添加登录信息
        // ！如果游戏在一定时间内无任何登陆，会被服务器自动删除
        $k_logins = new SAE_VALUE('server', 'total_logins');
        $total_logins = $k_logins->get(0);
        $total_logins += 1;
        $k_logins->update($total_logins);
        // 后续处理
        $k_aeskey = new SAE_VALUE($gid, '#aeskey');
        $k_aeskey->update($recv['aeskey']);
        $url = channel_url($gid, $uid, 600);
        return [0, ['gid' => $gid, 'url' => $url]];
    }

    // group 操作    
    $gid = $recv['gid'];

    // 一次性获取全部数据
    if ($recv['action'] === 'get_data') {
        $data = group_keys($gid);
        $n = strlen(DB . $gid);

        $data = array_filter($data, function ($k) use ($n) {
            return substr($k, $n + 3, 1) !== '#';
        }, ARRAY_FILTER_USE_KEY);

        $keys = array_map(function ($k) use ($n) {
            return substr($k, $n + 3);
        }, array_keys($data));
        $values = array_values($data);
        return [0, ['gid' => $gid, 'keys' => $keys, 'values' => $values, 't' => microtime(true)]];
    }

    // Get File
    if ($recv['action'] === 'get_file') {
        $contents = get_file($gid, $recv['path']);
        return [-1, $contents];
    }

    // List File
    if ($recv['action'] === 'list_file') {
        $stor = new sinacloud\sae\Storage();
        $list = $stor->getBucket(Bucket, DB . $gid . '/');
        return [0, ['gid' => $gid, 'info' => $list]];
    }

    // 以下操作需要已登录才可以继续执行    
    // 写文件    
    $k_online = new SAE_VALUE($gid, 'online');
    $online = array_unique($k_online->get());
    if (!in_array($recv["uid"], $online)) {
        return [3, "not in group"];
    }

    // Put File
    if ($recv['action'] === 'put_file') {
        put_file($gid, $recv['path'], $recv['content']);
        return [0, ['gid' => $gid, 'path' => $recv['path']]];
    }

    return [4, "invalid action"];
}

// --------------------------------------------------------
// rsa / aes / md5 等加密相关的函数
// --------------------------------------------------------
function sshKeyGen(): array
{
    $config = array(
        "digest_alg" => "sha512",
        "private_key_bits" => 4096,
        "private_key_type" => OPENSSL_KEYTYPE_RSA,
    );
    $res = openssl_pkey_new($config);
    openssl_pkey_export($res, $privKey);
    $pubKey = sshEncodePublicKey($res);
    return [$privKey, $pubKey];
}

function sshEncodePublicKey($privKey)
{
    $keyInfo = openssl_pkey_get_details($privKey);
    $buffer = pack("N", 7) . "ssh-rsa" .
        sshEncodeBuffer($keyInfo['rsa']['e']) .
        sshEncodeBuffer($keyInfo['rsa']['n']);
    return "ssh-rsa " . base64_encode($buffer);
}

function sshEncodeBuffer($buffer)
{
    $len = strlen($buffer);
    if (ord($buffer[0]) & 0x80) {
        $len++;
        $buffer = "\x00" . $buffer;
    }
    return pack("Na*", $len, $buffer);
}

function getRSAKey(): string
{
    $stor = new sinacloud\sae\Storage();
    @$response = $stor->getObject('rsa', DB . 'id_rsa');
    if ($response !== false) {
        return $response->body;
    } else {
        return "";
    }
}

function rsa_decode(string $base64_data): array
{
    $data = base64_decode($base64_data);
    if (openssl_private_decrypt($data, $decrypted, RSA_PRIVATE_KEY, OPENSSL_PKCS1_OAEP_PADDING)) {
        return json_decode($decrypted, true);
    } else {
        return [];
    }
}

function rsa_encode(array $data): string
{
    $json = json_encode($data);
    openssl_private_encrypt($json, $crypted, RSA_PRIVATE_KEY, OPENSSL_NO_PADDING);
    return base64_encode($crypted);
}

# input type: array, output type: base64 data
function aes_encode(array $data, string $key): string
{
    $iv = openssl_random_pseudo_bytes(16);
    $ret = json_encode($data);
    $ret = gzcompress($ret);
    $ret = openssl_random_pseudo_bytes(16) . $ret;
    $ret = openssl_encrypt($ret, 'aes-128-cbc', $key, OPENSSL_RAW_DATA, $iv);
    return base64_encode($ret);
}

# input type: base64 data, output type: array
function aes_decode(string $data, string $key): array
{
    $iv = openssl_random_pseudo_bytes(16);
    $ret = base64_decode($data);
    $ret = openssl_decrypt($ret, 'aes-128-cbc', $key, OPENSSL_RAW_DATA, $iv);
    $ret = substr($ret, 16);
    $ret = gzuncompress($ret);
    return json_decode($ret, true);
}

function aes_encrypt(string $data, string $key): string
{
    $iv = openssl_random_pseudo_bytes(16);
    $ret = openssl_random_pseudo_bytes(16) . $data;
    return openssl_encrypt($ret, 'aes-128-cbc', $key, OPENSSL_RAW_DATA, $iv);
}

function aes_decrypt(string $data, string $key): string
{
    $iv = openssl_random_pseudo_bytes(16);
    $ret = openssl_decrypt($data, 'aes-128-cbc', $key, OPENSSL_RAW_DATA, $iv);
    return substr($ret, 16);
}

function checksum(string $data): string
{
    return hash_hmac("md5", $data, SAE_ACCESSKEY, false);
}
 