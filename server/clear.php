<?php
if (!is_https()) {
    echo "<blockquote>For your security, this website requires HTTPS.</blockquote>";
    $url = "https://" . $_SERVER['HTTP_HOST'];
    die("<p>Goto: <a href=\"${url}\">${url}</a></p>");
}

if (!isset($_GET['db'])) {
    die("no db in query string");
}

require 'base.php';

$valid = false;
if ($_SERVER['PHP_AUTH_USER'] == $_GET['db']) {
    // 管理员模式
    if ($_SERVER['PHP_AUTH_PW'] == checksum(SAE_SECRETKEY)) {
        $valid = true;
    }
    // 密码登陆
    $k = new SAE_VALUE('server', '#password');
    if (checksum($_SERVER['PHP_AUTH_PW']) == $k->get("")) {
        $valid = true;
    }
}

# gid
$gid = $_GET['gid'];
if (!$gid) {
    $hint = 'RESET RSA KEY';
} else {
    if ($gid === 'server') {
        $hint = 'CLEAR THE GAME';
    } else {
        $hint = "CLEAR [${gid}]";
    }
}

# HTTP 基础认证
if (!$valid) {
    header('WWW-Authenticate: Basic realm="' . $hint . '"');
    header('HTTP/1.0 401 Unauthorized');
    echo "<h1>Not authorized</h1>";
    die("<a href=\"/\">Back</a>");
}

echo "<h1>" . $hint . "</h1>";

# 删除 keys & files
if ($gid) {
    if ($gid === 'server') {
        $keys = clear_group("");
    } else {
        $keys = clear_group($gid);
    }
}

# 删除 rsa key
if (!$gid || $gid === 'server') {
    $stor = new sinacloud\sae\Storage();
    $stor->deleteObject('rsa', DB . 'id_rsa');
}
if (!$gid) {
    die("<a href=\"/" . $_GET['db'] . "/server\">Refresh</a><br/>");
} else {
    die("<a href=\"/\">Back</a>");
}
