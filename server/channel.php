<?php
$client = $_POST['from'];
define(DB, explode("/", $client)[0] . "/");

require "base.php";

$uid = client2uid($client);
$action = $_GET['action'];
switch ($action) {
    case 'connected':
        $recv = ['action' => 'channel_connected'];
        break;
    case 'message':
        $k_aeskey = new SAE_VALUE("u-$uid", "#aeskey");
        define(AES_KEY, base64_decode($k_aeskey->get("")));
        $recv = recvdata($_POST['message']);
        break;
    case 'disconnected':
        $recv = ['action' => 'channel_disconnected'];
        break;
    default:
        die();
}

if (!is_array($recv)) {
    die();
}

$recv += ['cid' => $uid, 'uid' => $uid];
$ret = rgsos_main_ws($recv);

$msg = "j=" . json_encode($ret);

// using encrypt
$key = md5(substr(DB, 0, -1), true);
$msg = "a=" . aes_encode($ret, $key);

if ($ret[0] == 0) {
    broadcast($recv['gid'], $msg, [$uid]);
}

$channel = new SaeChannel();
$channel->sendMessage($client, $msg);
