<?php
if (!is_https()) {
    echo "<blockquote>For your security, this website requires HTTPS.</blockquote>";
    $url = "https://" . $_SERVER['HTTP_HOST'];
    die("<p>Goto: <a href=\"${url}\">${url}</a></p>");
}

if (!isset($_GET['db'])) {
    die("no db in query string");
}

require "base.php";

if (preg_match("/^[\w|-]+$/", $_GET['db']) == 0) {
    echo "<h1>Bad game name.</h1>";
    die("<a href=\"/\">Back</a>");
}

if (!getRSAKey()) {
    $stor = new sinacloud\sae\Storage();
    $list = $stor->getBucket("rsa", "");
    if (sizeof($list, 0) >= Max_Game_Num) {
        echo "<h1>Too much games.</h1>";
        die("<a href=\"/\">Back</a>");
    }

    list($pubKey, $password) = reset_game();
    echo "<a href=\"/" . DB . "server\">Refresh</a><br/>";
    die($pubKey . "<br/>" . $password);
}

?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <?php
    $gid = $_GET['gid'];

    echo "<title>GroupPanel - $gid</title>";
    ?>

    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://cdn.bootcss.com/github-markdown-css/2.10.0/github-markdown.css" rel="stylesheet">
    <style>
        .markdown-body {
            box-sizing: border-box;
            min-width: 480px;
            max-width: 1440px;
            margin: 0 auto;
            padding: 45px;
        }
    </style>
</head>

<body>
    <article class="markdown-body">

        <?php
        function inspect($obj): string
        {
            return htmlspecialchars(json_encode($obj));
        }
        if ($gid) {
            $keys = group_keys($gid . '/');
        } else {
            $keys = [];
        }

        $data = keys2data($gid, $keys);

        if ($_GET['debug'] == 1) {
            echo "<p>keys</p><pre>";
            echo inspect($keys);
            echo "</pre>";
            foreach ($data as $key => $value) {
                echo "<p>$key</p><pre>";
                echo inspect($value);
                echo "</pre>";
            }
        }

        // 输出内容
        echo "<h1>" . DB . $gid . "</h1>";
        $db = $_GET["db"];

        if ($gid == 'server') {
            echo "<a href=\"/clear.php?db=${db}\">RESET RSA KEY</a><br/>";
            $hint = "CLEAR ALL THE GAME";
        } else {
            $hint = "CLEAR GROUP: ${gid}";
        }

        echo "<a href=\"/clear.php?db=${db}&gid=${gid}\">${hint}</a>";

        echo "<h2>STATE</h2>";
        foreach ($data['state'] as $name => $contents) {
            echo "<h3>$name</h3>";
            echo "<table>";
            echo "<tr><th>uid</th>";

            $keys = [];
            foreach ($contents as $uid => $info) {
                $keys = array_merge($keys, array_keys($info));
            }
            $keys = array_unique(array_values($keys));
            asort($keys);
            foreach ($keys as $k) {
                echo "<th>$k</th>";
            }
            echo "</tr>";

            foreach ($contents as $uid => $info) {
                echo "<tr>";
                echo "<td>$uid</td>";
                foreach ($keys as $k) {
                    echo "<td>" . inspect($info[$k]) . "</td>";
                }
                echo "</tr>";
            }
            echo "</table>";
        }

        echo "<h2>VALUE</h2>";
        if (!empty($data['value'])) {
            echo "<table>";
            echo "<tr><th>key</th><th>value</th></tr>";
            foreach ($data['value'] as $k => $v) {
                if (substr($k, 0, 1) == '#') {
                    $value = str_repeat('#', 16);
                } else {
                    $value = inspect($v);
                }
                echo "<tr><td>" . $k . "</td><td>" . $value . "</td></tr>";
            }
            echo "</table>";
        }

        $stor = new sinacloud\sae\Storage();
        $list = $stor->getBucket(Bucket, DB . $gid . '/');

        echo "<h2>FILE</h2>";
        if (!empty($list)) {
            echo "<table>";
            foreach (array_keys(array_values($list)[0]) as $k) {
                echo "<th>$k</th>";
            }
            echo "</tr>";

            foreach ($list as $info) {
                echo "<tr>";
                foreach ($info as $v) {
                    echo "<td>$v</td>";
                }
                echo "</tr>";
            }
            echo "</table>";
        }
        ?>

    </article>
</body>

</html> 