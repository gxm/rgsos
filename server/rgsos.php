<?php
require "base.php";

define(RSA_PRIVATE_KEY, getRSAKey());

if (!RSA_PRIVATE_KEY) {
    die("Game not exist.");
}

// 文件处理的情况
if (isset($_FILES['file'])) {
    $recv = [
        'action' => 'put_file',
        'content' => file_get_contents($_FILES['file']['tmp_name']),
    ];
    $recv += recvdata($_POST['json']);
    $recv['content'] = aes_decrypt($recv['content'], base64_decode($recv['aeskey']));
} else {
    $recv = recvdata();
}

$ret = rgsos_main_http($recv);

$key = md5(substr(DB, 0, -1), true);

if ($ret[0] < 0) {
    die("t=" . aes_encrypt($ret[1], $key));
} else {
    die("a=" . aes_encode($ret, $key));
}
