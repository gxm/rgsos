# RGSOS Wiki
[https://gitlab.com/gxm/rgsos/wikis/home](https://gitlab.com/gxm/rgsos/wikis/home)

##  书写说明
1. 最后一段文字要少于 4 行
2. 代码后要紧接文字，不能空行

## 功能简介
`RGSOS` (Ruby Game Server On SAE) 是为 RMXP / VX / VA
提供网络服务的脚本。
包括服务端代码和 RMXP / VX / VA 客户端的范例。

```ruby
# update in each frame
loop do
  # ...
  $net.update
end
```
RGSOS 在后台自动运行，完成注册、登陆、同步数据。
**本地数据会自动更新**，游戏里直接读取本地数据即可。
开发者、用户无需关心 RGSOS 的具体实现细节。

除了**访问网络**的功能之外，同时还附带了必不可少的
**后台运行**、**输入法**、**数据加密**等功能。

您可以跟随此教程学习 RGSOS 的使用。
首先了解一下这些基本功能吧！

## 通讯协议
RGSOS 使用了 AsHttp 库发送 HTTP 请求，
此外，也能使用 WebSocket 协议传输信息。
WebSocket 协议支持服务端推送消息，同时传输也更快。

```ruby
# Send HTTP Request
Net_HTTP::connect("http://www.baidu.com", "", "GET") do |data|
  p data
end
```
比如，发送一个 HTTP 请求的代码显示在控制台里了。
当然也可以发送 POST 请求，并在受到请求后执行回调。

与 HTTP 协议相比，WebSocket 更轻巧更快，
同时也支持服务端的主动推送，
是实时游戏、聊天室等广泛运用的通讯协议。

```ruby
# Connect to WebSocket Server
Net_WebSocket::connect("echo", "ws://echo.websocket.org") do |data|
  p data
end
Net_WebSocket::send_data("echo", "hello, websocket")
```
这是连接到一个 WebSocket 服务器的代码。
**注意，connect 方法的执行需要较长时间。**

**SAE 对 WebSocket 的数据大小限制为 4kB。**
超过 4kB 的数据上传、下载仍需借助 HTTP。

SAE 文档中提到 WebSocket 链接只能持续 2 小时，
但实际测试可以持续超过 24 小时都不会断开。
不过，即使断开了，RGSOS 也会断线重连，无需担心。

RGSOS 对这两个通讯协议做了更多的封装，
包括序列化、压缩、加密、错误码、回调等。
普通的开发者无需考虑这些细节。

但是开发者需要知道以下 2 点：
1. **网络通讯存在延迟**，通常不会少于 2 帧 (=30ms)
2. 处理的数据较大时，程序会掉帧、卡顿

## 输入法
RGSOS 使用了夏娜输入法。
RGD <= 1.3.2 的版本与此输入法有冲突，
请使用 RGD 的最新版本。

在地图画面按下回车就可以打开输入框。
输入任意文字后，再次按下回车触发回调。
输入框为空的时候，按下回车会关闭输入框。

一个重要的设计是：
**输入框打开等价于对话的消息框打开。**
为了避免冲突，对话的消息框打开时，无法打开输入法。
显然，输入法打开时，无法通过键盘下达任何指令。

```ruby
# use typefield in interpreter
$scene.open_typefield do |text|
  p text
  $scene.close_typefield
end
```
可以很容易的在事件脚本里打开输入法，
让用户输入任意的内容并执行后续回调。
注意 VA 没有 `$scene`，而是 `SceneManager.scene`

最后，可能有的输入法会导致卡顿。
推荐使用微软输入法。

## 后台运行
后台运行使用了 SixRC 的后台运行脚本。
即使用户不进行操作，网络脚本也需要保持运行，
以使得本地的数据及时与服务器同步。

请删掉其他的后台运行脚本，
并把这个脚本尽量放到脚本编辑器器的最前面。

在出现标题画面前，不要让程序处于后台，
方可使得后台运行脚本正常运行。

## 控制台
用调试模式打开游戏会同时打开控制台，
设置 $output_level = -1 -> 3
控制台会输出不同详细级别的信息。

打包后的游戏无法打开调试模式，
并且固定 $output_level = -1。

```ruby
# hook update of all scenes
alias _net_base_update update

def update
  _net_base_update
  $net.update
  debug(binding) if Input.trigger?(Input::F8) && $output_level > 0
end
```
scene_base_net 里修改了所有场景的 update 方法，
每帧自动执行 $net.update。
同时也允许按下 F8 暂停游戏，焦点切换到控制台。

在控制台输入任意的 ruby 代码执行。
即使代码报错也不会使游戏崩溃，可以放心调试。

## JSON格式
JSON 是 javascript 语言使用的序列化格式。
也是被各种语言广泛支持的格式。
RGSOS 通过 JSON 格式的字符串与服务端交换数据。

JSON 值可以是 5 种简单类型：
数字（整数或浮点数），字符串， true / false / nil
和 2 种复合类型：数组和对象

并且，数组和对象里的值，只能是上面的简单类型，或者
另一个数组或对象。
其中对象类似于 ruby 里的 Hash，但是要求 key 也必须是
字符串。

RGSOS 使用了 OkJson 库进行序列化，所有满足以上要求
的 Array 和 Hash 对象都可以正常传递给服务器。

基于 OkJson 库的实现，无法传递非 UTF-8 字符。
举例，无法直接将 marshal 字符串传递，
请使用 base64 编码后再传递。

访问此链接以获得更多的信息：
`http://www.runoob.com/json/json-tutorial.html`

## 数据安全
RGSOS 的运行流程通常是这样的：
1. 玩家使用 http 登陆到服务器
2. 服务器返回 websocket 地址
3. 玩家连接此地址，与服务器进行通讯

在第 1 步里，玩家会用 RSA 公钥加密自己的登陆信息，
包括：用户ID、密码和随机的 websocket 通讯密码。
此信息只有服务器能解密。

在第 2 步里，仅有一个地址返回。
**返回数据用游戏统一密码加密。**
游戏统一密码是游戏名的 md5 值。

在第 3 步里，玩家发送的上行数据，是用 websocket 
通讯密码加密的，但是收到的数据，
则是用游戏统一密码加密。

以后的版本可能考虑下行数据也使用 websocket 通讯
密码。

## 部署脚本
注意到教程的编辑器里没有任何脚本，而是通过
`load "main.rb"` 或 `Kernel.load "main.rb"`
来加载外部脚本。

建议开一个空的工程，并且
1. 复制 System 文件夹到工程目录下
2. 复制 tutorial.md 文件到工程目录下
3. 复制脚本到 F11 编辑器

将脚本移动到 F11 编辑器的方法是：
从 main.rb 的第 1 行开始，逐行复制。

如果遇到了 load "xxx.rb" 或者 Kernel.load，
新建一个新的脚本页，复制 xxx.rb 里的内容。
然后换一页继续。

xxx.rb 在教程的 Scripts 目录下，
或者在外面的 Basic_Scripts 目录下。

注释掉 `scene_nettest.rb` 对应的脚本。
因为 main.rb 没有导入此脚本，那一行被注释掉了。

```txt
https://gitlab.com/gxm/rgsos/blob/master/tutorial.md
```
你也许注意到了，教程的工程内没有任何信息，
所有的对话都是读取 tutorial.md 文件里的内容。
你可以直接查看 tutorial.md，或者访问：
`https://gitlab.com/gxm/rgsos/blob/master/tutorial.md`

```ruby
class Tutorial
  Path_MD = "tutorial.md"  
  # ...
end
```
修改 tutorial.rb 中的 Path_MD 指向工程目录下的
tutorial.md 文件。

如果教程运行正常，说明脚本已经成功移动到编辑器
里了。

通常情况下，RGSOS 能够兼容您的游戏。
但最好不要急着去把脚本移植到您的游戏里，
而是跟随教程进入下一张地图继续学习。

## 注册游戏
看上去你已经新建了一个工程并且成功复制了全部的代码。
接下来需要做的是去 SAE 上注册账号，并且部署服务端。
或者，访问 `https://rgsos.applinzi.com`。

在 Create Game 下面的输入框里输入游戏名，
注意只能是字母、数字、下划线(_)和短横线(-)
按下回车后，网站会显示服务器公钥和密码。

复制显示的内容，然后点击刷新，游戏就创建好了！
此时网站跳转到此游戏的 server 群组。

RGSOS 上不同游戏名的数据是隔离的，同一个游戏里以
群组划分数据的访问权限。这点后面会详谈。

当你在网站上进行操作时，可能要提供凭据，
输入游戏名和服务器密码即可。
**不要泄露服务器密码给任何人。**

## 更换密钥
你应该注意到了，main.rb 最上方的常量 RSA_KEY，
这是区分不同游戏的服务器公钥，请修改这个公钥获得
自己独一无二的游戏。

```ruby
RSA_KEY = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQC1a9jI4RRH73jVA/xqq5rfEqRxb2YS8JTr/DGSctyR6PWjAFBKNgUbJUjXUab6oeIuLqvZcf42VnGtbIOG/eKLuAZ/52lEDh/vA7eHZXbYEyL0xdDfc8vSb3d8kyDcoOsc8rohoZafpg6lId6c8kUxfQZ0Jy6PTTn0TGjy+6IvI6VYlz20/qRJwlYITFwXGPYW3/Gk9bpqrkLSdNxu1RQj4mC31fA8IF+dwl4j+j90jxwTelIEfvKqhLy8hjIStSugHuR+K1f+PD30gEqkqP2WLpFX+VBhnHorpHLwu5/YXpqjRP8saFyt1X+DBzdmF5cEXBNrfQxsQ8ub+Pukqji/d51RB+Frfk9lk9ZErCZJ/8SpQVSYy65WFhVm069dSSvdPZWOHAV5/TRLjnp34yKA8TXZPmGLORpED4kM68aBA8qcmUeuNeTBgEJYoS8XXsvHK/q13xNkzaBttStxmAtPLHD3jjb/oQ4TDcxENLJ38ukqQd0OTUooDxuCLROqMRm50GvNS/bU49hp6I0v4G19iAKVx05/s629ok0cnCMu8vj/ivH9PftH4lyyXOWOSg/Prpk3iMcLHjYncUWVr0y6cTD3V4fx7lzzr1MR+pnfWct9DmI6EGF2YVPIJkYtULM913R5hhh9H39hGlPnddBsTf5ufpr0F4O6zQ2pd1AQ0w== example-vx"
```
重置本游戏的 RSA_KEY = 复制的那部分内容：
以 ssh-rsa 开头，以游戏名结束的一行字符串。
如果在编辑器中不是 1 行，请删掉所有的换行符。

```ruby
$net = Network_Base.new
$net.server_url = "http://rgssser.applinzi.com"
```
另外，把 main.rb 最下方，也就是编辑器中最后一段，
关于服务器的位置改为：
`$net.server_url = "http://rgsos.applinzi.com"`

再次打开游戏，就能看到自己成为第一个注册的玩家。
访问 `https://rgsos.applinzi.com/游戏名/server`
能看到 total_users = 1

## 自制游戏
更换 RSA_KEY 之后，就可以把这些脚本加入到自己的游戏里。
但是还有一些地方要注意：

1. 移除 scene_nettest 和 tutorial，包括 tutorial.md
2. 发行版要移除 user 文件
3. 提醒玩家无法打开的情况下，尝试修改兼容性设置

RGSOS 在 win10 上开发，但应该对其他 windows 兼容。
如果程序假死、闪退，试一下修改兼容性设置。

接下来就可以在自制游戏中进行开发、测试了！
当然，也可以跟随教程进一步学习。

在网站上可以删除自己的游戏，
或者重置服务器公钥和密码。
重置后，使用旧的公钥就无法登陆了。

## 群组
前面提到过，游戏里的数据按照群组分开。
可以类比成 QQ 群，需要进群才能聊天、分享文件。
此外，玩家可以同时加入任意多个群组。

群组里的变量、文件可以直接访问网站进行速览：
`https://rgsos.applinzi.com/游戏名/群组名`
由于网址出现了 /，群组名里不能使用 /。
进一步的，在群组里的变量名也不能使用 /。

RGSOS 会自动把玩家添加到 u-X 群组，
其中 X 是玩家的 id。
访问此网站看看自己当前是否在线吧。

```ruby
# join a group
$net.join_group("test")
# exit a group
$net.exit_group("test")
# object of test group
$net.groups["test"]
$net.groups["test"].data
# my home
$net.home == $net.groups["u-#{$net.uid}"]
```
加入、退出群组，访问群组内变量非常简单。
群组里的变量会自动同步以保证最新。
RGSOS 里标记用户的私人群 u-X 为 **home**。

在加入群组后，RGSOS 会尝试同步群组里的全部变量，
同步在加入的 1 秒后启动。但是单次同步很快。

比如 u-X 群里的 name，在获取到此数据后，
才会在角色脚下显示名字，而同步延迟启动导致：
角色脚下的名字不会立刻出现，但修改起来很快。

```ruby
# wait for online
loop do 
  # ...
  if $net.online?
    break
  end
end
```
如果有可能，在 `$net.online?` 为 true 之前，
一直停留在载入画面。不这么做问题也不大，
比如本教程就没有这么做。

如果群组设置了密码，则需要正确的密码才能加入。
群组名以下划线(_)开头的是**临时群组**。
在所有玩家都退出后，临时群组里的数据、文件清空。

## 文件
登陆到群组里的成员，可以上传、下载文件。
最容易想到的就是云存档。

```ruby
# upload a file
File.open("Save1.rvdata", "r") do |f|
  $net.home.putF("save1.dat", f.read) {
    p "File uploaded."
  }
end
```
比如，给用户的私人群上传自己的存档。
如果上传的数据是空字符串，则会删掉此文件。

```ruby
# download a file
$net.home.getF("save1.dat") { |data|
  File.open("Save1.rvdata", "w") do |f|
    f << data
  end
}
```
或者，下载存档并保存到本地。
如果此文件不存在，则会获得空字符串。

由于文件通常比较大，每次操作都会花费较长时间。
**RGSOS 不会自动对文件进行同步操作。**

## 变量
变量分为两种：数值(Value)和状态(State)。
变量名须指定为 Symbol，也兼容使用字符串。
**变量值必须是 JSON 格式**，所以有时候需要完成
ruby 对象与 JSON 字符串之间的互转。

```ruby
# setup keys before join group
keys = Network_Group::Group_Keys
keys["test"] = {
  "value" => [:name, :array],
  "state" => [:info]
}
$net.join_group("test")
```
在 join_group 之前，需要**声明此群组内的变量名**，
否则无法对这些变量进行正常的设置和同步。
在 ready 后群组里的变量会自动同步以保证最新。

在没有得到服务器上的数据时，
数值变量默认是空数组 []
状态变量默认是空Hash {}
状态其实是 2 层hash，第一层的 key 是 uid。

```ruby
# set or unset a value
test = $net.groups["test"]
test.setV(:name, "new_name", "set")
test.setV(:name, "new_name", "unset")
```
**数值在服务器上永久保存。**
数值支持 set 操作。
set：设置此值

```ruby
# push or delete a value
test = $net.groups["test"]
test.setV(:array, [], "set")
test.setV(:array, 1, "push")
test.setV(:array, 1, "diff")
```
如果数值为空，或者已经被 set 成为数组，
还支持 push/diff 操作。
push：在数组的尾部添加目标值
diff：删掉数组中全部目标值

```ruby
test = $net.groups["test"]
test.get(:name)
test.get(:array)
test.data[:name]
test.data[:array]
```
变量存储在群组的 data 属性中，可以直接访问。
但推荐使用 get 访问对应的变量。
如果变量是数组或 Hash，请小心不要修改此变量。

```ruby
# fast set
test = $net.groups["test"]
test.set(:name, "new_name")
test.set(:info, "gold" => 100)
```
RGSOS 提供了简易的 set 方法，会根据此 key 的类型
自动判断使用 setV 或者 setS，并且
默认 setV 的第 3 个参数为 "set"
默认 setS 的第 3 个参数为 $net.uid

```ruby
# method_missing
test = $net.groups["test"]
test.name
test.name = "new_name"
test.info = {"gold" => 100}
```
同样，允许像使用属性一样使用群组的变量。
会等同于调用了 get / set 这两个方法。

尽管数值和状态和本地的变量不同，
更新有延迟，存储的内容本身也有诸多的限制。
但合理利用数值和状态，仍可以做出复杂的效果。

## 状态变量
```txt
# the state :info just likes a table:
 uid | name | gold | level  ...
-----+------+------+------- ...
   0 | "do" |  -1  |    10  ...
   1 | "re" | 100  |     1  ...
  99 | "mi" |   0  |     1  ...
```
状态看上去像是二维的表格，其首列为 uid。
状态最适用于快速更新的、只要最终结果的数据。
比如，角色在地图上的坐标。

由于其不限行数的表格形式，
状态也适合存储结构相似的系列变量，
比如，排行榜，新闻，开关。
请控制状态变量的大小 < 10kB (marshal_dump 后)。

```ruby
# set state
test = $net.groups["test"]
test.setS(:info, {"gold" => 100, "level" => 100}, 0)
test.setS(:info, {"gold" => nil}, 0)
```
操作状态需要提供第 3 个参数 uid。
**状态只有覆盖和删除操作。**
设置 key 为 nil 即删除这个 state 表中的一格。
**状态无法整行删除**，即使你删掉了该行的全部元素。

```ruby
# dup all states
$net.send_ws("test", "action" => "dup_state")
```
允许手动永久存储群组里全部状态。
此外，在所有人都退出群组后，状态也会永久存储。
但是状态名以下划线(_)开头的状态是临时状态，
会在所有人都退出群组后，自动删除。

```ruby
# local state data
test = $net.groups["test"]
info = test.get(:info)
info["1"]["gold"]
```
在本地的状态数据，是以 uid 为索引的 hash。
其值也是 hash。切记要用字符串作为 hash 的索引。

## 监听回调
除了直接访问群组的 data 属性以获取最新的数据外，
还可以利用监听来完成 key 变动的实时回调。

由于只在数据更新的时候执行回调代码块，
以及其他的一些优化，监听回调会**略微**提升性能。

```ruby
# listening a key of group
$net.listen("test", :array) { |g, k, v|
  p "group:#{g}'s #{k} has been set to #{v}"
}
```
比如，监听群组 "test" 里的 :array，
一旦发生了变化就执行 block 里的内容。
listen 的第 1 个参数也可以是正则表达式。

```ruby
# show listening keys
$net.news
$net.news["test"].delete(:array)
$net.news["test"].clear
$net.news.delete("test")
```
其实 listen 只是在往 $net 的 news 属性里添加值。
news 是一个双层的 hash，可以删掉特定的值以停止监听。

```ruby
# listening news
class Network_Base
  def update
    Net_HTTP::update
    Net_WebSocket::update
    return if !online?
    @groups.values.each do |g|
      if g.ready
        g.update
        update_news(g.gid, g.news)
        g.news.clear
      end
    end
  end
end
```
方法是检查 $net.groups 中的全部群组的 news 属性。
这里的 news 属性是数组，值是所有本帧内改变的 key。

```ruby
$net.listen("test", :F) { |g, k, v|
  p "File is ready"
}
$net.groups["test"].putF("a.txt", "contents") { |error, data|
  $net.groups["test"].news.push(:F)
}
```
允许监听不存在的 key，只需要在特定的时候，
往群组的 news 属性里 push 此 key 即可。
比如，文件传输结束的时候触发回调。

在某个群组的 news 处理完毕后，news 会清空。
从而在监听的回调代码里给 news 添加 key 
以触发同组另一个监听事件无任何效果。

请注意回调代码块内各变量的**作用域**。
如果不明白什么是作用域，请使用**全局变量**。

不掌握此写法也没有关系，只需要在有需要的时候
访问对应群组的 data 属性获取变量的值即可。
注意变量在第一次同步前，默认是空的数组或Hash。

## 高级教程
以下是高级教程，会详细说明：
1. state 和 value 的具体实现
2. 教程中位置同步、聊天功能的实现

## KV数据库
K-V 存储系统是最简单的数据库类型之一。
几乎所有的编程语言都带有内置的 K-V 存储功能。
比如，ruby 里的 Hash，
再比如，RGSS 里的 $game_variables。

由于以下 2 点原因，RGSOS 没有使用 SQL 数据库：
1. SAE 上的 MySQL 实例收取租金 ~15 RMB / 月
2. 对于小型游戏，K-V 型数据库更合适。

KV 数据库无法对 value 进行查找，
除非把所有的值都取出来筛选其中满足要求的。
对 KV 数据库的操作需要已知 key。

RGSOS 中，某个数值对应的 key 值为：
data/游戏名/群组名/V-数值变量名，
比如 `data/test/server/V-total_users`

由于使用了斜线(/)作为分隔符，
禁止在游戏名、群组名、变量名中使用斜线(/)

状态并不是对应单个 key，
而是多个以 uid 结尾的 key：
data/游戏名/群组名/S-状态变量名/uid，
比如 `data/test/test/S-move/1`

虽然写作 uid，但是可以不是玩家的 uid。
比如，排行榜的名次 uid = 1 ~ 99

状态变量的过期时间是 24 小时，
是因为状态变量存储在 **Memcached** 中。
状态不适合设置后就不常更新的数据类型。

状态变量是**无锁**的，这意味着高并发操作会导致
不可预知的结果，甚至不同客户端会收到不同的数据。
**请在应用层阻止多人同时操作同一个状态变量。**
比如，只操作对应自己 uid 的状态变量。

数值变量存储在硬盘中，修改操作有锁。
通常情况下不知道用那种，设置成数值变量准没错。

所有的变量在本地都会有默认的过期时间，
过期后会要求服务器群发此变量的最新版本强制同步。
开发者需要控制使用的变量数量、大小。

## 位置同步
接下来介绍教程里的位置同步是怎么写的。
学习这段内容，需要一定的编程知识，
并且前面的教程也都大致看过一遍。

位置同步需要做到以下几件事：
1. 建立地图群组
2. 获取在线玩家
3. 模拟玩家行动

任务1. 建立地图群组 开始！
考虑到可能需要多个地图处于同一个群组，
需要判断当前的 `$game_map.map_id`，
获取对应的群组名。

```ruby
# multi maps share same group
class Game_Map
  def net_gid
    case @map_id
    when 1
      "_map1"
    when 2, 3
      "_map2"
    else
      "_map3"
    end
  end
end
```
这段代码写在 `class Game_Map` 里最合适。
比如，第 1 张地图单独是一个群组 "_map1"，
第 2、3 张地图同属一个群组 "_map2"。
其他的地图就都划到一个群组算了 "_map3"。

然后，在加入地图的时候，顺便加入对应的群组。
这段代码以钩子方法写在 `Game_Map#setup` 中。
请自行查看。

任务1. 建立地图群组 完成！
使用临时群组的原因是，
1. 服务端不关心客户端上传的位置是否合法
2. 服务端只是转发，不需要存储位置信息

任务2. 获取在线玩家 开始！
RGSOS 为每个群组添加了默认的 online 变量。
在客户端发送 join_group 指令的时候自动修改。

```ruby
# /_map\d+/ keys
class Network_Group
  Group_Keys[/_map\d+/] = {
    "value" => [:online, :chat],
    "state" => [:player],
  }
end
```
只需要在脚本里声明，/_map\d+/ 类型的群组的变量：
包括 online，chat 和 player。
后面 2 个变量用于聊天和上传位置状态。

```ruby
# listen the key online
class Network_Base
  # ...
  def initialize
    # ...
    listen(/_map\d+/, :online) { |g, k, v|
      if g == @map_group.gid
        self.map_online = v.uniq
      end
    }
  end
  # ...
end
```
需要监听 online， 一旦有其他玩家加入或者离开，
游戏应该做出一些应对。这段代码以钩子方法
写在 `Network_Base#initialize` 中。

给 `Nework_Base` 创建属性 map_players，
用于管理其他玩家的数据。
map_players 是 hash，其键为 uid，
其值是 `Game_NetPlayer` 对象。

```ruby
# calling back when online changes
def map_online=(online)
  old_online = @map_players.keys
  # player join
  (online - old_online).each do |uid|
    next if uid == self.uid  
    @map_players[uid] = Game_NetPlayer.new(uid)    
  end
  # player exit
  (old_online - online).each do |uid|        
    @map_players.delete(uid)
  end
end
```
在 `Network_Base#map_online=` 方法中，
根据新的 online 和之前 online 的异同，
向 map_players 里添加 / 删除 `Game_NetPlayer` 对象。

任务2. 获取在线玩家 完成！
接下来需要仔细设计 `Game_NetPlayer` 类，
以达到成功模拟另一个玩家行走的效果。

任务3. 模拟玩家行动 开始！
注意到我们已经完成的工作：
1. 群组 /_map\d+/ 中有状态变量 player
2. 新玩家加入地图时，会创建 `Game_NetPlayer` 对象

要做的事情就比较清晰了：
1. `Game_NetPlayer` 继承 Game_Character 类
2. 其属性跟随 map.data["player"][uid] 修改
3. 在 Scene_Map 里创建 Sprite_Character

```ruby
# initialize net player
class Game_NetPlayer < Game_Character
  def initialize(uid)
    super()
    @uid = uid
    @pos = [0, 0, 0, 0] # map_id, x, y, direction
    @through = true
  end
end
```
毕竟是个弱的同步，其他的玩家不能影响本地游戏。
设置穿透为 ON，并且传递独立的 uid 为初始化参数。

```ruby
# update properties in each frame
def update
  super
  self.player_data = $net.map_group.get("player")[@uid] || {}
  move
end
```
在 update 方法里，每帧读取本地保存的 player 数据，
并修改 `Game_NetPlayer` 的相关属性。
这里没有使用监听回调，而是仿照 RGSS 的写法。

```ruby
# move towards target
def move
  unless moving? || @transparent
    # ...
  end
end
```
为了使显示流畅，`Game_NetPlayer` 不会立刻出现在
其他玩家上传的位置，而是慢慢走到目标点。
这段代码直接复制了 RGSS 里事件靠近主角的代码，
在 RMXP / VX / VA 里略有不同。

上面只有收到数据后的处理，还需要玩家主动把
自己的位置数据上传。
这一段代码以钩子方法写在 `Game_Player#update` 中

接下来就是创建 Sprite_Character 对象，
在地图上显示其他的玩家。
在监听 online 的 `Network_Base#map_online=` 里
执行对应操作即可。

任务3. 模拟玩家行动 完成！
接下来就是一些细节的调整，隐藏不在当前地图的角色，
以及添上名字和聊天显示，这些相对简单很多。

## 观察模式
即使没有声明对应的数值、状态变量，
群组内所有变量也会在加入群组时一次性获得并存储，
可以修改变量，但是本地的数据不会随之更新。

此外，对于状态，加入群组获取的并不是实时值，
而是上一次 dup 的值，dup 可以手动触发，
或者所有人都退出群组时自动 dup。

设置群组里数值变量 password，
之后其他人再加入此群组都需要同样的密码。
但是超级用户 u-0 无需密码就可以加入群组。
超级用户的密码就是服务器密码。

任何人都无法以任何手段获得 password 的值，
但是在群组中的用户可以 unset password。

使用 join_group 加入群组时提供了错误的密码，
将导致此用户处于**观察模式**，
用户的 uid 不会加入到 online 变量里。
用户将无法修改变量，也无法上传、下载文件。

但是本地仍然会创建 Network_Group 对象，
用户可以获得变量的值。

注意，此时变量会自动过期并同步，
但不会在改变的时候立刻收到推送。
可以手动发送 `get_value / get_state` 请求更新。

结合以上 2 种情况，如果未声明任何变量，
并以错误密码加入一个设有密码的群组，会导致：
1. 加入群组后获得当时的全部变量数据
2. 这些变量数据永远不会更新

用途1：
想查看特定玩家是否在线 (online)，
或者在哪个地图 (group) 时。
可以加入此好友的用户群组读取。

用途2：
获取游戏的新闻、排行榜、工会信息等内容。
可以放到群组 server 里，另开一个群组也行。
