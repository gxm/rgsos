# rgsos 架构

## 重要提醒
由于SAE的服务政策发生了巨大的变化，已经无法自行搭建服务器。
- [https://rgsos.applinzi.com](https://rgsos.applinzi.com)仍然可以使用
- 本工程整合了大量的脚本，可供研究联网技术的开发者参考学习

## 简介
`RGSOS` (Ruby Game Server On SAE) 是一个简单的网络服务，部署在 SAE 上。其特点为：
1. 支持 `RMXP/VX/VA` 版本，提供了客户端的范例脚本，性能良好
2. 使用 `websocket` 长连接进行通讯，较大的文件支持使用 HTTP 上传
3. 弱服务端，服务端几乎不进行任何验证操作，只是暂存、广播客户端上传的数据
4. 同一个服务端支持多个游戏，每个游戏的数据以“群组”为单元

## 使用说明
### 注册游戏
1. 在SAE上部署服务端脚本（见后），或者访问 [https://rgsos.applinzi.com](https://rgsos.applinzi.com)    
2. 输入游戏名创建游戏，此时服务器会返回以下数据：
    - RSA 公钥
    - 服务器密码
3. 记录下此数据，刷新页面后你将永远不会看到这两个数据    

### 配置客户端
1. 注意按照 `main.rb` 脚本的顺序，依次拷贝脚本到脚本编辑器(F11)中。
2. 设置 `RSA_KEY`
3. 设置 `$net.server_url = `，请使用 `rgsos` 替代 `rgssser`
    - `rgssser` 是开发者用来测试的网站
4. 调试模式测试游戏是否能正常运行
    - 控制台正常打开
    - 显示 `register` 或者 `login`
    - 显示 `connected after xxx s`
    - 游戏目录下出现 `user` 文件
    - 进入地图画面后显示 `join_group`
    - 角色脚下显示名字
    - 按下回车打开输入框，输入内容后回车会显示在角色头上
4. 将 `user` 中的 `id` 改成 0，密码改成服务器密码，再次登陆
    - 正常登陆，此时是管理员用户，此用户 `join_group` 无需提供密码
    - 请勿泄露服务器密码，`main.rb` 中能看到仅方便测试，部署时务必删除

### 测试客户端
1. 复制客户端，测试联机效果
2. 挂机足够长时间，测试稳定性
3. 测试脚本性能（见后）

### 服务器数据
1. 访问 `https://rgsos.applinzi.com/{game_name}/server` 查看注册的玩家总数
2. 上面的 `server` 替换成 `u-{uid}` 查看玩家的信息
3. `server` 替换成其他的 `group_id` 以查看对应的群组信息
4. 在群组页面，允许清空整个群组
5. 在 `server` 页面，允许清除整个游戏，或者更换 RSA 公钥和服务器密码
    - 必须使用游戏名，服务器密码进行权限验证

## RGD 支持插件
> warning: `RGD <= 1.3.2` 和输入法脚本冲突

如果RGD在电脑上无法正常执行，显示缺少DirectX组件，访问下面的网站下载：
- [DirectX组件](https://www.microsoft.com/zh-cn/download/details.aspx?id=35)

## 自行部署
1. 注册SAE账号，充值，创建新SAE应用
    - PHP7.0
    - 标准环境
    - Git
2. 运行环境管理 -> 代码管理 -> 创建新版本 -> 1
3. 选择上传代码包，将`server`下的全部文件打包上传
    - 上传失败请用 Git 上传
3. 选择在线编辑，修改 `base.php` 中的 `Max_Game_Num`
4. 数据库与缓存：启用 KVDB，启用 Memcached，容量设置为 16M
5. 存储与CDN：在 Storage 里创建 2 个 bucket：data 和 rsa，高级设置中设置 bucket 的权限为“私有”
6. 通讯服务：打开 Channel 服务
7. 在脚本 `main.rb` 里修改 `$net.server_url = ` 后的网站名
8. （可选）进行实名制认证

## 添加新的功能
> 请仔细阅读以 `Scene_` 开头的数个脚本，了解加入群组、监听-回调等写法

我们以在地图上同步玩家的位置为例，介绍群组的相关操作
1. 定义若干个群组：`_map\d+`，包括其需要同步的 `key` 和需要监听的 `key`
    - 只要配置里没有的 `key` 在游戏里无法读、写，比如 `password`
    - 监听的 `key` 发生变化时会执行回调，比如 `online` 和 `chat`
2. 在进入地图的时候加入群组并退出其他的地图群，调用 
```ruby
$net.join_group(current_gid)
$net.exit_group(other_gid)
```
3. 当 `$game_player` 的数据发生变化时，上传对应的数据
```ruby
$net.map_group.set('player', net_player_data)
```
4. 除了监听-回调之外，也可以定时检查地图里的同步数据，进行相应的设置
    - 比如状态 `player`，每帧都从 `$net.map_groups.data['player']` 中获取
    - 禁止对 `data` 进行改写！如果必须要用到破坏性的修改，请 clone 一个对象
    - 比起监听-回调，这样会消耗更多的资源，但通常不会影响游戏性能

## 监测性能
可以在`scene_net_base.rb` 的 `$net.update` 前后使用以下代码监控性能：
```ruby
# ...
t = Time.now.to_f
$net.update
dt = Time.now.to_f - t
p dt if dt > 0.001
# ...
```
实测大部分时候都在 1ms 内执行完毕。但是要注意的是，非监听-回调的部分并不在 `$net.update` 里执行。

## 临时群组和变量
群组名以 `_` 开头的是临时群组，当所有的玩家都离开群组后，里面的内容会立即清空

非临时群组的状态(State)类型的变量名，如果是以 `_` 开头，会在所有的玩家都离开后清空，否则永久的存储下来

注意
1. State类型的变量是保存在 Memcached 中，只在所有玩家都离开后 dup 到 KVDB 里，故在网站上不会即刻更新
2. State类型的变量会在 24h 后过期删除，手动保存的方法是：
```ruby
$net.send_ws(gid, "action" => "dup_state")
```
