# encoding: UTF-8
# 用你服务器公钥替换此公钥
# 同时修改服务器网址
module RGSOS
  RSA_KEY = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQDNmhprdsmf5tZkn+Qe2bLC83dYjJ1EXeFAcGyvIDo7S6o3S8BqEixBWpib2m+Xnq8Qrvzcyo8df+HqqfiCDYqitVsbjCDPVx0G3/g0OG6e8aifT6VYl2BrFTw7t4Bn6qcmY5UoR2t2e3+XSRGE5mDXfbp+5pw9z0v8p4Qcxm/sIaC7Qw9wEeu1ijzcZptwPsKO5TQOMlH1HjY9xSDyYy8vOhnM6xRyH/Qo2tmDkwZHjQbzyyK4QPGg9xFx00iCRnn1vk0e67PCgGOlNVVnGFG0sBP/gbos6oe8/tsQTispZWUWkkz8x4J+85eWKVnDv9M2Zlb2/gS5yiSicBJI9/tqH2+68aJi7sHOgyN8pbEB54H4y8GC3RXqsXsByTW8+F8S4TrLtTrHmtyXYfGaxdhBMyECCgMY4GqHfvzVCAACLzhfe6kMKYt8VhQMMv0rrwssYC2KpMe8RbczrSC9nUB5EKYaAKJyq5o3g2mPB0ztCkb7J/3g67TfZiLL03QuocZEGj1lp/JFXldY1yPMUTCPicQtcurjbQTOhIEms+TtPJX80yZNrme/8pTfdPu/OV049n5sag22ATq95EuIdK5Lc2MEHCzXHClyA3snB2eKYeslRIideb4ZjTprrUbbGRH8fp0yefBiSjuTI49qSLozuemSG5wG8ft38CJFErasww== client-vx"
  SERVER_URL = "http://rgssser.applinzi.com"
end

# 这里写的是服务器密码，只是为了测试方便
# 禁止把自己的服务器密码写在游戏里
"75bd601d1a486d1f934bea310aa71039"

# 游戏设置
Kernel.load "game_config.rb"
# 控制台与后台运行
Kernel.load "console.rb"
Kernel.load "background.rb"
# JSON
Kernel.load "okjson.rb"
Kernel.load "okjson_rm.rb"
# RSA / AES 加密
Kernel.load "encrypt.rb"
# http / websocket
Kernel.load "ashttp.rb"
Kernel.load "net_http.rb"
Kernel.load "net_websocket.rb"
# 标准 network
Kernel.load "network_base.rb"
Kernel.load "network_group.rb"
# 输入法(需要和 map_input 结合起来用)
Kernel.load "type_field.rb"

# scene内运行，同步地图位置、显示名字和聊天信息
Kernel.load "scene_base_net.rb"
Kernel.load "scene_map_net_base.rb"
Kernel.load "scene_map_net.rb"
Kernel.load "scene_map_net_input.rb"
Kernel.load "scene_map_net_name.rb"

# Kernel.load "scene_nettest.rb"
Kernel.load "tutorial.rb"

Kernel.load "network_begin.rb"
