# encoding: UTF-8
#==============================================================================
# ■ Scene_Base
#------------------------------------------------------------------------------
# 　游戏中所有 Scene 类（场景类）的父类
#==============================================================================

class Scene_Base
  #--------------------------------------------------------------------------
  # ● 更新画面（基础）
  #--------------------------------------------------------------------------
  alias _net_base_update update

  def update
    _net_base_update
    $net.update
    debug(binding) if Input.trigger?(Input::F8) && $output_level > 0
  end

  alias _net_base_pre_terminate pre_terminate

  def pre_terminate
    _net_base_pre_terminate
    if $scene == nil
      $net.logout
    end
  end
end
