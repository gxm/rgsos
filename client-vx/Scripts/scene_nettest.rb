# encoding: UTF-8

class Network_Group
  Group_Keys[/test/] = {
    "value" => [:online, :rand],
    "state" => [:move],
  }
end

class Scene_Title < Scene_Base
  alias _net_test_update update

  def update
    _net_test_update
    $net.online? ? @i += 1 : @i = 0
    # ---------------------------------------------
    # * test
    # ---------------------------------------------
    if @i == 1
      $net.join_group("test") # test
      $net.listen("test", :F) { |g, k, v|
        p "@#{@i}, File is ready"
      }
      $net.listen("test", :online) { |g, k, v|
        p "@#{@i}, online: #{v}"
      }
      $net.listen("test", :move) { |g, k, v|
        for uid, data in v
          p "@#{@i}, move [#{uid}] to [#{data["x"]}, #{data["y"]}]"
        end
      }
      $net.listen("test", :rand) { |g, k, v|
        p "@#{@i}, rand: #{v}"
      }
    end
    if @i == 100
      $net.groups["test"].putF("a.txt", "中文测试之测试2") { |error, data|
        $net.groups["test"].news.push(:F)
      }
    end
    if @i == 200
      $net.groups["test"].getF("a.txt") { |data|
        data.force_encoding("UTF-8") if RUBY_19
        p "@#{@i}, #{data}"
      }
      $net.groups["test"].listF { |error, data|
        p data
      }
    end
    if @i % 50 == 10 && @i < 200
      $net.groups["test"].setS(:move, {"x" => rand(10), "y" => rand(10)}, nil)
      # $net.groups["test"].setV(:password, "test", "set")
    end
    if @i == 400
      p $net.groups["test"].data
      $net.exit_group("test")
    end
    if @i == 600
      $scene = nil
    end
    # ---------------------------------------------
  end

  def pre_terminate
    $net.logout
  end
end
