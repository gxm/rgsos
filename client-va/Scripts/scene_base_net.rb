# encoding: UTF-8
#==============================================================================
# ■ Scene_Base
#------------------------------------------------------------------------------
# 　游戏中所有 Scene 类（场景类）的父类
#==============================================================================

class Scene_Base
  #--------------------------------------------------------------------------
  # ● 更新画面（基础）
  #--------------------------------------------------------------------------
  alias _net_base_update_basic update_basic

  def update_basic
    _net_base_update_basic
    $net.update
    debug(binding) if Input.trigger?(Input::F8) && $output_level > 0
  end
end

module SceneManager
  def self.exit
    @scene = nil
    $net.logout
  end
end
