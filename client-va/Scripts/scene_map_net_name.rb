# encoding: UTF-8

class Game_Player < Game_Character
  attr_accessor :net_chat

  def net_name
    ($net.online?) ? $net.home.get(:name) : ""
  end

  alias _net_map_name_net_player_data net_player_data

  def net_player_data
    data = _net_map_name_net_player_data
    data.update("name" => $game_player.net_name)
    data
  end
end

class Game_NetPlayer < Game_Character
  attr_accessor :net_chat

  def net_name
    player = $net.map_group.get(:player)
    if player && player[@uid]
      return player[@uid]["name"]
    else
      return nil
    end
  end
end

class Sprite_Character < Sprite_Base
  #--------------------------------------------------------------------------
  # ● 更新源位图（Source Bitmap）
  #--------------------------------------------------------------------------
  alias _net_map_name_update update

  def update
    _net_map_name_update
    update_net_name
    update_net_chat
  end

  def update_net_name
    return if !defined? @character.net_name
    return if !@character.net_name
    if @net_name != @character.net_name
      @net_name = @character.net_name
      bitmap = Bitmap.new(96, 32)
      bitmap.font.size = 18
      bitmap.draw_text(bitmap.rect, @net_name, 1)
      @sprite_name ||= ::Sprite.new(viewport)
      @sprite_name.bitmap = bitmap
      @sprite_name.ox = bitmap.width / 2
      @sprite_name.visible = false
    elsif @sprite_name
      @sprite_name.x = x
      @sprite_name.y = y
      @sprite_name.z = z
      @sprite_name.visible = visible
    end
  end

  def update_net_chat
    return if !defined? @character.net_chat
    return if !@character.net_chat
    if @net_chat != @character.net_chat
      @net_chat = @character.net_chat
      bitmap = Bitmap.new(320, 32)
      bitmap.font.size = 20
      bitmap.draw_text(bitmap.rect, "[ %s ]" % @net_chat, 1)
      @sprite_chat ||= ::Sprite.new(viewport)
      @sprite_chat.bitmap = bitmap
      @sprite_chat.ox = bitmap.width / 2
      @sprite_chat.visible = false
      @net_chat_counts = 300
    elsif @sprite_chat
      @sprite_chat.x = x
      @sprite_chat.y = y - 64
      @sprite_chat.z = z + 2
      @sprite_chat.visible = visible
      if visible
        @net_chat_counts -= 1
      else
        @net_chat_counts = 0
      end
      if @net_chat_counts == 0
        @character.net_chat = nil
        @sprite_chat.dispose
        @sprite_chat = nil
      end
    end
  end

  alias _net_map_name_dispose dispose

  def dispose
    if @sprite_name
      @sprite_name.dispose
      @sprite_name = nil
    end
    if @sprite_chat
      @sprite_chat.dispose
      @sprite_chat = nil
    end
    _net_map_name_dispose
  end
end

class Network_Group
  Group_Keys[/u-\d+/]["value"] ||= []
  Group_Keys[/u-\d+/]["value"].push :name
end

class Scene_Map < Scene_Base
  def produce_cmd(text)
    a = text.split(" ")
    case a[0]
    when "-b"
      produce_chat("balloon_id" => a[1].to_i)
    when "-n"
      $net.home.set("name", a[1].to_s[0..32])
    end
  end
end
