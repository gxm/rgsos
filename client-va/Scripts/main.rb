# encoding: UTF-8
# 用你服务器公钥替换此公钥
# 同时修改服务器网址
module RGSOS
  RSA_KEY = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQDMud/HDyW0VFgKE2a8ggWSSN2kzGLX+kHa+RxBWBn+xMLX3c1N11SUvPQT/Hm9YFzmurXr2yn7bxeKtx8erpJ1GIASHh91XPRGl+t6BkqtR65YeWvDH+tZBgUTX2qobAerzox7Ov0Tn/kvYR3yrc0BFAPsKeSG6ExagY+bESPXjz7+uEcCPM2MBj9nsuev7fb85bz2/yD5Qd36Y66Hrt+hpi6ITvx3tlxeqEOx4OB/RvF2Vchqsdqp9YD+QzfdlqmHlNkugi5UqKFmQdR/WOavSpw0TC4mkjOW/kYQ4npnsz4MW2hbtuANvNn9+69Cdf6/HhnwXwCNmULQ4JXc60P9bQcjH9NxGvn2MvQ1ikDemsuzHw+2ojlrC/zIM+1db1065Gl81D+N9+6CYBqkoIpaW3zobPTI2zDsaYjYcIWWfm6OSVVuS7l4iipLQea9jAB5sdzLQfbGTgatUZC/TvGMs8qqRFZ5sPgxXVthjqddqHcCm0F0vXWpcJ+giJbm21kwHC5+2NGiUSZ0nDEcXd6/qlA8/IcUWgAsEUbwKBIIK8F3YRErGerAWCR8pSFjgsMqNWsggcg3qMLCvDP8DiEmN/nJ5SLD0pBYFzG+LwmrafMeDqNZH2rU7iZOcR7AGFAUBcUG2zAujWwrLVaY4Zx9GmrylVHEtzrC1albd+Rkpw== client-va"
  SERVER_URL = "http://rgssser.applinzi.com"
end

# 这里写的是服务器密码，只是为了测试方便
# 禁止把自己的服务器密码写在游戏里
"332c2ccca40b4492be5dfa59edce975f"

# 游戏设置
load "game_config.rb"
# 控制台与后台运行
load "console.rb"
load "background.rb"
# JSON
load "okjson.rb"
load "okjson_rm.rb"
# RSA / AES 加密
load "encrypt.rb"
# http / websocket
load "ashttp.rb"
load "net_http.rb"
load "net_websocket.rb"
# 标准 network
load "network_base.rb"
load "network_group.rb"
# 输入法(需要和 map_input 结合起来用)
load "type_field.rb"

# scene内运行，同步地图位置、显示名字和聊天信息
load "scene_base_net.rb"
load "scene_map_net_base.rb"
load "scene_map_net.rb"
load "scene_map_net_input.rb"
load "scene_map_net_name.rb"

# load "scene_nettest.rb"
load "tutorial.rb"

load "network_begin.rb"
