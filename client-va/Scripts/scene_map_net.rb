# encoding: UTF-8

class Network_Base
  # online 变化时 @map_players 改变， 同时调用当前场景的对应方法
  def map_online=(online)
    old_online = @map_players.keys
    # 新加入的玩家
    (online - old_online).each do |uid|
      next if uid == self.uid
      p "player [#{uid}] join" if $output_level > -1
      @map_players[uid] = Game_NetPlayer.new(uid)
      SceneManager.scene.net_add_player(@map_players[uid])
    end
    # 已经退出的玩家
    (old_online - online).each do |uid|
      p "player [#{uid}] exit" if $output_level > -1
      SceneManager.scene.net_del_player(@map_players[uid])
      @map_players.delete(uid)
    end
  end

  def map_chat=(chat)
    return if !chat.is_a? Hash
    # 对 chat 的处理
    uid = chat["uid"]
    t = chat["SERVER_TIME_STAMP"]
    player = (uid == $net.uid) ? $game_player : @map_players[uid]
    if player && near_time?(t)
      if player == $game_player || player.in_map?
        if chat["balloon_id"]
          player.balloon_id = chat["balloon_id"]
        end
        if chat["msg"]
          player.net_chat = chat["msg"]
        end
      end
    end
  end
end

class Game_Player < Game_Character
  def net_player_data
    data = {}
    data["looks"] = [@character_name, @character_index]
    if transfer?
      data["pos"] = [@new_map_id, @new_x, @new_y, @new_direction]
    else
      data["pos"] = [$game_map.map_id, @x, @y, @direction]
    end
    data
  end
end

class Game_NetPlayer < Game_Character
  def move
    unless moving? || @transparent
      sx = distance_x_from(@pos[1])
      sy = distance_y_from(@pos[2])
      if sx != 0 && sy != 0
        move_diagonal(sx > 0 ? 4 : 6, sy > 0 ? 8 : 2)
      elsif sx != 0
        move_straight(sx > 0 ? 4 : 6)
      elsif sy != 0
        move_straight(sy > 0 ? 8 : 2)
      else
        @direction = @pos[3]
      end
    end
  end
end

# Scene_Base 中定义的方法会在 online 数据变化时调用
# Scene_Map 会改写此方法立即刷新 @character_sprites
class Scene_Base
  def net_add_player(player); end
  def net_del_player(player); end
end

# Scene_Map 需要定义添加、删除 sprite 的方法
# 在 update 方法里定期更新 $net.map_players
# [测试]: 提供了一个 trigger 触发气泡
class Scene_Map < Scene_Base
  alias _net_map_update update

  def update
    _net_map_update
    g = $net.map_group
    if g && g.ready
      $net.map_players.values.each do |player|
        player.update
      end
    end
  end

  def net_add_player(player)
    @spriteset.net_add_player(player)
  end

  def net_del_player(player)
    @spriteset.net_del_player(player)
  end
end

# Spriteset_Map 跟随 Scene_Map 定义了添加、删除 sprite 的方法
# 在创建 character 的时候，顺便添加 $net.map_players 里的内容
class Spriteset_Map
  alias _net_map_create_characters create_characters

  def create_characters
    _net_map_create_characters
    $net.map_players.values.each do |player|
      sprite = Sprite_Character.new(@viewport1, player)
      @character_sprites.push(sprite)
      player.update
    end
  end

  def net_add_player(player)
    @character_sprites.push(Sprite_Character.new(@viewport1, player))
    update_characters
  end

  def net_del_player(player)
    sprite = @character_sprites.find { |sprite|
      sprite.character == player
    }
    if sprite && !sprite.disposed?
      sprite.dispose
      @character_sprites.delete(sprite)
      update_characters
    end
  end
end
