# encoding: UTF-8

class Scene_Base
  def open_typefield(text = "", &block)
    @viewport_input = Viewport.new(96, 360, 352, 28)
    @viewport_input.color.set(0, 0, 0, 120)
    @viewport_input.z = 300
    @type_field = Type_Field.new(@viewport_input, text, nil, 22)
    @type_field.active = true
    @type_field.callback = block
    $game_message.visible = true
  end

  def close_typefield
    TypeAPI.lostFocus
    @type_field_proc = nil
    $game_message.visible = false
    @type_field.active = false
    @type_field.dispose
    @viewport_input.dispose
    @type_field = nil
    @viewport_input = nil
    return true # true 表示立刻退出 TypeField 的 update
  end

  def type_field_busy?
    @type_field && @type_field.active
  end

  alias _net_map_input_pre_terminate pre_terminate

  def pre_terminate
    _net_map_input_pre_terminate
    close_typefield if @type_field
  end
end

class Scene_Map < Scene_Base
  alias _net_map_input_update update

  def update
    _net_map_input_update
    # 这里保持使用 RInput, 即使在RGD下也能正常执行
    if RInput.trigger?(RInput::ENTER)
      if !$game_message.busy?
        open_typefield { |text|
          if text.empty?
            close_typefield
          else
            if text[0] == "-"
              produce_cmd(text)
            else
              produce_chat("msg" => text)
            end
          end
        }
      end
    end
    @type_field.update if @type_field
  end

  def produce_chat(data)
    data.update("uid" => $net.uid, "SERVER_TIME_STAMP" => true)
    $net.map_group.set(:chat, data)
  end

  def produce_cmd(text)
  end
end

class Game_Message
  alias _net_map_input_busy? busy?

  def busy?
    _net_map_input_busy? || SceneManager.scene.type_field_busy?
  end
end
