# encoding: UTF-8

Scene_Base.each do
  #-------------------------------------
  # class eval
  #-------------------------------------
  def open_typefield(text = "", &block)
    @viewport_input = Viewport.new(96, 360, 352, 28)
    @viewport_input.color.set(0, 0, 0, 120)
    @viewport_input.z = 300
    @type_field = Type_Field.new(@viewport_input, text, nil, 22)
    @type_field.active = true
    @type_field.callback = block
    $game_temp.message_window_showing = true
    # 防止在输入法打开时，触发地图上的事件
    if self.is_a? Scene_Map
      $game_system.map_interpreter.message_waiting = true
    end
  end

  def close_typefield
    TypeAPI.lostFocus
    @type_field_proc = nil
    if self.is_a? Scene_Map
      $game_system.map_interpreter.message_waiting = false
    end
    $game_temp.message_window_showing = false
    @type_field.active = false
    @type_field.dispose
    @viewport_input.dispose
    @type_field = nil
    @viewport_input = nil

    return true # true 表示立刻退出 TypeField 的 update
  end

  alias _net_map_input_pre_terminate pre_terminate

  def pre_terminate
    _net_map_input_pre_terminate
    close_typefield if @type_field
  end
end

class Scene_Map
  alias _net_map_input_update update

  def update
    _net_map_input_update
    if RInput.trigger?(RInput::ENTER)
      if !$game_temp.message_window_showing
        open_typefield { |text|
          if text.empty?
            close_typefield
          else
            if text[0].chr == "-" # xp
              produce_cmd(text)
            else
              produce_chat("msg" => text)
            end
          end
        }
      end
    end
    @type_field.update if @type_field
  end

  def produce_chat(data)
    data.update("uid" => $net.uid, "SERVER_TIME_STAMP" => true)
    $net.map_group.set(:chat, data)
  end

  def produce_cmd(text)
  end
end

#==============================================================================
# ■ Window_Message
#------------------------------------------------------------------------------
# 　显示文章的信息窗口。
#==============================================================================

class Window_Message < Window_Selectable
  alias _net_map_input_update update

  def update
    _net_map_input_update
    if $scene.instance_variable_get(:@type_field)
      $game_temp.message_window_showing = true
    end
  end
end

class Interpreter
  attr_accessor :message_waiting
end
