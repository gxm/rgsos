# encoding: UTF-8
#==============================================================================
# ■ Scene_Base
#------------------------------------------------------------------------------
# 　游戏中所有 Scene 类（场景类）的父类
#   RMXP 中无此设置，故对所有以 Scene_xxx 开头的常量都进行了设置
#   由于对所有的 Scene_xxx 都做了操作，此脚本要尽量在所有的 Scene_xxx 之后
#==============================================================================
module Scene_Base
  module_function
  All_Scenes = Class.constants.select { |c|
    c =~ /^Scene_/ && Class.const_get(c).is_a?(Class)
  }
  All_Scenes -= [Scene_File]

  def each(&block)
    All_Scenes.each do |scene|
      Class.const_get(scene).class_eval(&block)
    end
  end
end

Scene_Base.each do
  alias _net_base_update update

  def update
    _net_base_update
    $net.update
    debug(binding) if Input.trigger?(Input::F8) && $output_level > 0
  end

  alias _net_base_main main

  def main
    _net_base_main
    pre_terminate
  end

  def pre_terminate
    if $scene == nil
      $net.logout
    end
  end
end
