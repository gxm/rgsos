# encoding: UTF-8
# 用你服务器公钥替换此公钥
# 同时修改服务器网址
module RGSOS
  RSA_KEY = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQDbRWTQJ/w0jy0CeRdCGzWkY4PEMTe3nfmcFvt1TnuTTjYipckkA3S6vk//KZyc3H9nB2ISpobbvQzFrIODzE7P/bAniDCM6q/HxnRL6ApYh9Vdc+/FzQ9qyNaBZJhVLy/bxx9RHNtKGyHoth/k/1r9WlAV6NaxWUFNnUZjbeocJrvIi0LNjszQspa192rb7ICRy4IIuo1WDaMb4y2vwjY/ekFq8iEq0Er8W9ztgClICqvfZgykDPKnVEi90cQYXb953aVHQXqiICTAkncpWTzYbmv7ajv539GSEoI/pXBwWWIiy1HDDurKglGD4cYTlmmgo+KyaDIS4EsBkF00YmrWNjM+FRvTBx+Q8AovotXOPhu3eJDgbuSsg3tLFc2NcBWZY7PJFbQRaQpANxz/ZEZhaDhZbUfZuj5HgM3buK9avRkRvh4gVgvCsMPonQ7Vju/0tt5H3NXu5bi8Y276+u2Fhp2YyLA+A3kvXfqIqLxyFiBxWeFgLpHCtSGUTsrLXYvQXJsWxfaxH7WvL/E9FvhB097Ox5XkA7pLd8ngM49hmTJPQeUTBjKcqzQhwBG77m3Nk/9I605ru+75jk3rB+VclkEG6l4Lt+enU+orbRtoGB02eOluAkDSO0xmrLWpmTl/pPDOlzQZFDZalaa2FGfUFEcYLI51hRGrNJSolLc3Jw== client-xp"
  SERVER_URL = "http://rgssser.applinzi.com"
end

# 这里写的是服务器密码，只是为了测试方便
# 禁止把自己的服务器密码写在游戏里
"ceb95a737837a39e8c5a3d513d116458"

# 游戏设置
load "game_config.rb"
# 控制台与后台运行
load "console.rb"
load "background.rb"
# JSON
load "okjson.rb"
load "okjson_rm.rb"
# RSA / AES 加密
load "encrypt.rb"
# http / websocket
load "ashttp.rb"
load "net_http.rb"
load "net_websocket.rb"
# 标准 network
load "network_base.rb"
load "network_group.rb"
# 输入法(需要和 map_input 结合起来用)
load "type_field.rb"

# scene内运行，同步地图位置、显示名字和聊天信息
load "scene_base_net.rb"
load "scene_map_net_base.rb"
load "scene_map_net.rb"
load "scene_map_net_input.rb"
load "scene_map_net_name.rb"

# load "scene_nettest.rb"
load "tutorial.rb"

load "network_begin.rb"
