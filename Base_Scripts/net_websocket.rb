# encoding: UTF-8

module Net_WebSocket
  DLL_PATH = "System/websocket.dll"

  WS_CONNECT = Win32API.new(DLL_PATH, "ws_connect", "ip", "i")
  WS_UPDATE = Win32API.new(DLL_PATH, "ws_update", "i", "i")
  WS_DATA = Win32API.new(DLL_PATH, "ws_data", "pp", "v")
  WS_SEND = Win32API.new(DLL_PATH, "ws_send", "ip", "v")
  WS_CLOSE = Win32API.new(DLL_PATH, "ws_close", "i", "v")

  Max_Channel_Num = 32
  Channels = {}

  def self.connect(name, ws_url, &block)
    if Channels.keys.include? name
      self.disconnect(name)
    end
    used_id = Channels.values.collect { |ws| ws.id }
    new_id = ((0...Max_Channel_Num).to_a - used_id).min
    raise if new_id == nil
    Channels[name] = Channel.new(new_id, ws_url, &block)
    return Channels[name]
  end

  def self.update
    Channels.each_pair do |name, ws|
      ws.update
      if ws.closed
        Channels.delete(name)
      end
    end
  end

  def self.send_data(name, data)
    if Channels[name]
      Channels[name].send(data)
    end
  end

  def self.disconnect(name)
    if Channels[name]
      Channels[name].disconnect
      Channels[name].update
    end
    Channels.delete(name)
  end

  class Channel
    attr_reader :id
    attr_reader :ws_url
    attr_reader :on_message_proc
    attr_reader :disconnect_proc
    attr_reader :closed
    attr_reader :key

    def initialize(id, ws_url, &block)
      @id = id
      if block_given?
        on_message(&block)
      end
      @closed = false
      WS_CONNECT.call(@id, ws_url)
    end

    def send(data)
      msg = encode(data)
      WS_SEND.call(@id, msg)
    end

    def update
      n = WS_UPDATE.call(@id)
      if n < 0
        disconnect
        return
      end
      buffer_len, recv_num = n / 1024, n % 1024
      if recv_num > 0
        buffer = "\0" * buffer_len
        lens = "\0" * (4 * recv_num)
        WS_DATA.call(buffer, lens)
        s = 0
        lens.unpack("L*").each do |len|
          handle_message(buffer[s...s + len])
          s += len
        end
      end
    end

    def disconnect
      return if @closed
      WS_CLOSE.call(@id)
      @disconnect_proc.call() if @disconnect_proc
      @closed = true
    end

    def on_message(&block)
      @on_message_proc = block
    end

    def on_disconnect(&block)
      @disconnect_proc = block
    end

    def handle_message(msg)
      data = decode(msg)
      @on_message_proc.call(data) if @on_message_proc
    end

    def encode(data)
      if data.is_a? String
        data
      else
        "a=" + AES.encode(data)
      end
    end

    def decode(data)
      if $output_level > 2
        File.open("ws.log", "a") { |f| f.puts data }
      end
      type = data[0...2]
      data.slice!(0...2)
      case type
      when "j="
        return OkJson.decode(data)
      when "t="
        return AES.decrypt(data)
      when "a="
        return AES.decode(data)
      else
        return type + data
      end
    end
  end
end
