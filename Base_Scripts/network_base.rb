# encoding: UTF-8

class Network_Base
  attr_reader :news
  attr_reader :server_url
  attr_reader :groups
  attr_reader :time_shift

  def initialize
    @groups = {}
    @news = {}
    @news_group_cache = {}
    @retry = 3
    @time_shift = 0
    if File.exist?(RGSOS::USER_PATH)
      data = File.open(RGSOS::USER_PATH, "r") do |f|
        OkJson.decode(f.read)
      end
      @uid = data["uid"]
      @password = data["password"]
      p "load uid = #{@uid}" if $output_level > -1
    end
  end

  def server_url=(server_url)
    if server_url[-1] != "/"
      server_url += "/"
    end
    @server_url = server_url + RGSOS::Game_Name
    # 注册|登录
    @uid ? login : register
  end

  def update
    Net_HTTP::update
    Net_WebSocket::update
    return if !online?
    @groups.values.each do |g|
      if g.ready
        g.update
        update_news(g.gid, g.news)
        g.news.clear
      end
    end
  end

  if defined? BGR
    alias _bgr_update update

    def update
      BGR.update
      _bgr_update
    end
  end

  def uid
    @uid.to_s
  end

  def home
    @groups["u-#{@uid}"]
  end

  def online?
    !!home && home.ready
  end

  # 注册
  def register
    p "register..." if $output_level > -1
    post_data = {"action" => "register"}
    Net_HTTP::connect(@server_url, post_data, "POST") do |error, data|
      if error == 0
        @uid = data["uid"]
        @password = data["password"]
        File.open(RGSOS::USER_PATH, "w") do |f|
          f << OkJson.encode({"uid" => @uid, "password" => @password})
        end
        p "save user: uid = #{@uid}" if $output_level > -1
        login
      else
        p "failed to regist: #{data}" if $output_level > -1
      end
    end
  end

  # 登录
  def login(delay = 0)
    p "login..." if $output_level > -1
    @retry -= 1
    @groups["u-#{@uid}"] = nil
    post_data = {"action" => "login", "uid" => @uid, "password" => @password, "aeskey" => AES.key}
    Net_HTTP::connect(@server_url, post_data, "POST", delay) do |error, data|
      if error == 0
        url = data["url"]
        @retry = 3
        @groups["u-#{@uid}"] = Network_Group.new("u-#{@uid}")
        # 注意，下面的 websocket 连接需要很长(~2s)的等待时间
        # 如果可能请使用 logo 展示进行掩盖
        p "connecting..." if $output_level > -1
        t = Time.now.to_f
        Net_WebSocket::connect("u-#{@uid}", url) { |error, data|
          gid = data["gid"]
          data.delete("gid")
          if @groups[gid]
            p "#{gid} | recv: #{OkJson.encode [error, data]}" if $output_level > 1
            @groups[gid].recv_ws(error, data)
          end
        }.on_disconnect {
          p "disconnect" if $output_level > -1
          if @retry > 0
            p "retry in 2 seconds" if $output_level > -1
            login(120)
          end
        }
        p "connected after %.2fs" % (Time.now.to_f - t) if $output_level > -1
        @groups.keys.each do |gid|
          next if gid == "u-#{@uid}"
          join_group(gid)
        end
      else
        p "failed to login: #{data}" if $output_level > -1
        if @retry > 0
          p "retry in 2 seconds" if $output_level > -1
          login(120)
        end
      end
    end
  end

  def logout
    p "logout..." if $output_level > -1
    @retry = 0
    @groups.keys.each do |gid|
      next if gid == "u-#{@uid}"
      exit_group(gid)
    end
    Net_WebSocket::disconnect("u-#{@uid}")
  end

  # 加入群组
  def join_group(gid, password = "")
    return if !online?
    p "#{gid} | join" if $output_level > -1
    send_ws(gid, "action" => "join_group", "password" => password)
    @groups[gid] = Network_Group.new(gid)
  end

  # 退出群组
  def exit_group(gid)
    return if !online?
    p "#{gid} | exit" if $output_level > -1
    send_ws(gid, "action" => "exit_group")
    @groups.delete(gid)
  end

  def send_ws(gid, data)
    return if !online?
    data["gid"] = gid
    p "#{gid} | send: #{OkJson.encode(data)}" if $output_level > 1
    Net_WebSocket::send_data("u-#{@uid}", data)
  end

  def gkeys
    Network_Group::Group_Keys
  end

  # 这里的 gid 可以是正则表达式
  def listen(gid, key, &block)
    @news[gid] ||= {}
    @news[gid][key.to_sym] = block
    # 一旦 @news 有新的监听，清空缓存
    # 如果试图取消监听，也要主动把缓存清除
    @news_group_cache.clear
  end

  def update_news(gid, keys)
    # 使用了缓存，避免多次正则表达式的匹配
    @news_group_cache[gid] ||= @news.keys.select { |g|
      g.is_a?(String) ? gid == g : gid =~ g
    }
    groups = @news_group_cache[gid]
    groups.each do |g|
      news = @news[g]
      # 这里使用 & 交集，自动去重
      (news.keys & keys).each do |k|
        news[k].call(gid, k, @groups[gid].get(k))
      end
    end
  end

  # 查看 http 访问延迟
  def delay
    Net_HTTP::Request.delay
  end

  def time
    @time_shift + Time.now.to_f
  end

  def near_time?(t, dt = 1)
    (t - time).abs < dt
  end

  def adjust_time(t)
    _time_shift = t - @time_shift - Time.now.to_f
    if _time_shift > 1
      @time_shift = _time_shift
    else
      @time_shift += _time_shift / 2
    end
  end
end
