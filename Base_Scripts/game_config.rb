# encoding: UTF-8

if !($TEST || $DEBUG)
  $output_level = -2
else
  $output_level = 0
  Dir["*.log"].each { |fn| File.delete(fn) }
end

RUBY_19 = String.method_defined? :force_encoding

if !String.method_defined? :bytesize
  String.class_eval {
    alias bytesize size
  }
end

if !defined? RGD
  Graphics.frame_rate = 60
end

module RGSOS
  VALUE_EXPIRE_FRAMES = 300
  HTTP_WAIT_FRAMES = 300

  if !defined? RSA_KEY
    File.open("id_rsa.pub", "r") do |f|
      RSA_KEY = f.read
    end
  end

  Game_Name = RSA_KEY.split(" ")[2]

  USER_PATH = "user"
end
