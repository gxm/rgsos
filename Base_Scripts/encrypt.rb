# encoding: UTF-8

module AES
  DLL_PATH = "System/cbc.dll"

  AES_CBC_ENCRYPT = Win32API.new(DLL_PATH, "aes_cbc_encrypt", "pppi", "v")
  AES_CBC_DECRYPT = Win32API.new(DLL_PATH, "aes_cbc_decrypt", "pppi", "v")
  # 下面 2 个实际上未用到
  AES_ECB_ENCRYPT = Win32API.new(DLL_PATH, "aes_ecb_encrypt", "ppi", "v")
  AES_ECB_DECRYPT = Win32API.new(DLL_PATH, "aes_ecb_decrypt", "ppi", "v")

  module_function

  def random_iv
    Array.new(16).map { rand(256) }.pack("C*")
  end

  def init_key
    @key = random_iv
    @key2 = (Hash2.update RGSOS::Game_Name, :md5)
  end

  def key
    Hash2.update @key, :encode
  end

  # input array, output Base64
  def encode(data)
    ret = OkJson.encode(data)
    ret = Hash2.update ret, :deflate
    ret = encrypt(ret)
    ret = Hash2.update ret, :encode
    ret
  end

  def decode(base64_string)
    ret = Hash2.update base64_string, :decode
    ret = decrypt(ret)
    ret = Hash2.update ret, :inflate
    ret.force_encoding("UTF-8") if RUBY_19
    ret = OkJson.decode(ret)
    ret
  end

  # tips
  # 这里直接对输入的字符串进行了 dup.force_encoding
  # 无论如何，返回的字符串的 encoding 是 ascii-8bit
  def encrypt(string, key = @key)
    str = string.dup
    str.force_encoding("ASCII-8BIT") if RUBY_19
    pad = 16 - str.bytesize % 16
    str = random_iv + str + ([pad] * pad).pack("C*")
    AES_CBC_ENCRYPT.call(key, random_iv, str, str.bytesize)
    return str
  end

  # 输出的数据如果要传给 OkJson，需要转换编码为 UTF-8
  # OkJson.decode(d.force_encoding("UTF-8"))
  def decrypt(string, key = @key2)
    iv = "\0" * 16
    str = string.dup
    AES_CBC_DECRYPT.call(key, iv, str, str.bytesize)
    str.force_encoding("ASCII-8BIT") if RUBY_19
    pad = str[-1]
    pad = pad.ord if RUBY_19
    return str[16...str.bytesize - pad]
  end

  # ecb length must be 16 * N
  def encrypt_ecb(str, key)
    if str.bytesize & 0x0f == 0
      AES_ECB_ENCRYPT.call(key, str, str.bytesize)
    end
  end

  def decrypt_ecb(str, key)
    if str.bytesize & 0x0f == 0
      AES_ECB_DECRYPT.call(key, str, str.bytesize)
    end
  end
end

#--------------------------------------------------
# Hash2
#--------------------------------------------------
module Hash2
  MD5Init = Win32API.new("advapi32", "MD5Init", "p", "v")
  MD5Update = Win32API.new("advapi32", "MD5Update", "ppi", "v")
  MD5Final = Win32API.new("advapi32", "MD5Final", "p", "v")

  A_SHAInit = Win32API.new("advapi32", "A_SHAInit", "p", "v")
  A_SHAUpdate = Win32API.new("advapi32", "A_SHAUpdate", "ppi", "v")
  A_SHAFinal = Win32API.new("advapi32", "A_SHAFinal", "pp", "v")

  module_function

  def update(string, method)
    string = string.dup
    string.force_encoding("ASCII-8BIT") if RUBY_19
    case method
    when :md5
      ctx = "\0" * 104
      MD5Init.call(ctx)
      MD5Update.call(ctx, string, string.bytesize)
      MD5Final.call(ctx)
      ctx.force_encoding("ASCII-8BIT") if RUBY_19
      return ctx[-16..-1]
    when :sha1
      ctx = "\0" * 84
      A_SHAInit.call(ctx)
      A_SHAUpdate.call(ctx, string, string.bytesize)
      buffer = "\0" * 20
      A_SHAFinal.call(ctx, buffer)
      return buffer
    when :crc32
      return Zlib::crc32(string).to_s(16)
    when :hash
      return string.hash.to_s(16)
    when :encode
      return [string].pack("m0")
    when :decode
      return string.unpack("m0").first
    when :bin2hex
      return string.unpack("H*").first
    when :hex2bin
      return [string].pack("H*")
    when :inflate
      return Zlib::Inflate.inflate(string)
    when :deflate
      return Zlib::Deflate.deflate(string)
    end
  end

  def update_file(filename, method)
    string = File.open(filename, "rb") do |file|
      file.read
    end
    update(string, method)
  end
end

#--------------------------------------------------
# CRC_Rand
#--------------------------------------------------
class CRC_Rand
  def initialize(seed = "")
    reset(seed)
  end

  def update(i = nil)
    @result = Zlib::crc32((@seed ^ @result).to_s(16))
    return (i ? @result * i : @result.to_f) / (1 << 32)
  end

  def reset(seed = "")
    @seed = Zlib::crc32(seed.to_s)
    @result = 0
  end
end

# ---------------------------------------------------------
# RSA
# ---------------------------------------------------------
# use:
# RSA.load_key('System/rsa.pub')
# RSA.encrypt_oaep(string) => base64_string
# RSA.encrypt_nopad(base64_string) => string
# ---------------------------------------------------------

module RSA
  # ---------------------------------------------------------
  module_function
  # ---------------------------------------------------------
  def load_key
    @e, @n = load_pub(RGSOS::RSA_KEY)
    @k = int2str(@n).bytesize
    @l = sha1("")
    if @e != 65537
      raise "failed to load rsa key: #{RSA_KEY}"
    end
  end

  def encrypt_oaep(data, base64_output = true)
    ps = "\0" * (@k - 42 - data.bytesize)
    db = @l + ps + "\x01" + data
    seed = Array.new(20).map { rand(256) }.pack("C*")
    db = xor(db, mgf1(seed, @k - 21))
    seed = xor(seed, mgf1(db, 20))
    em = "\x00" + seed + db
    em = Hash2.update(em, :encode)
    return encrypt_nopad(em, base64_output)
  end

  def encrypt_nopad(data, base64_output = false)
    data = Hash2.update(data, :decode)
    data = str2int(data)
    data = modpow(data, @e, @n)
    data = int2str(data)
    data = Hash2.update(data, :encode) if base64_output
    return data
  end

  # ---------------------------------------------------------

  def int2str(i)
    hex = i.to_s(16)
    hex = "0" + hex if hex.bytesize % 2 == 1
    Hash2.update(hex, :hex2bin)
  end

  def str2int(s)
    (Hash2.update(s, :bin2hex)).to_i(16)
  end

  def load_pub(string)
    s = Hash2.update(string.split(" ")[1], :decode)

    i = 0
    vars = []
    while i < s.length
      length = str2int(s[i...i + 4])
      i += 4
      vars.push s[i...i + length]
      i += length
    end

    if i == s.bytesize
      e, n = str2int(vars[1]), str2int(vars[2])
      return [e, n]
    else
      return false
    end
  end

  def egcd(a, b)
    if a == 0
      [b, 0, 1]
    else
      g, y, x = egcd(b % a, a)
      [g, x - (b / a) * y, y]
    end
  end

  def modinv(a, m)
    g, x, y = egcd(a, m)
    if g == 1
      x % m
    else
      raise
    end
  end

  def modpow(a, b, n)
    rec = 1
    while (b != 0)
      if b & 1 != 0
        rec = (rec * a) % n
      end
      a = (a * a) % n
      b = b >> 1
    end
    rec % n
  end

  def i2osp(i, len)
    a = []
    len.times do
      a.push(i & 255)
      i = i >> 8
    end
    a.reverse.pack("C*")
  end

  alias os2ip str2int

  def xor(s1, s2)
    raise if s1.bytesize != s2.bytesize
    int2str(str2int(s1) ^ str2int(s2))
  end

  def sha1(s)
    Hash2.update(s, :sha1)
  end

  def mgf1(s, l)
    t = ""
    for i in 0..(l / 20) # 20 is sha1 output length
      t << sha1(s + i2osp(i, 4))
    end
    t.force_encoding("ASCII-8BIT") if RUBY_19
    t[0...l]
  end
end
