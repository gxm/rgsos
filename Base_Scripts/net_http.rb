# encoding: UTF-8

# 发送 HTTP 请求，并且执行回调。 update 方法需要定时调用
module Net_HTTP
  STATE_WAIT = 0
  STATE_FINISH = 1
  STATE_ERROR = 2
  STATE_CLOSE = 3

  Requests = []

  def self.connect(url, data, method = "GET", delay = 0, &block)
    Requests.push Request.new(url, data, method, delay, &block)
  end

  def self.update
    Requests.each do |req|
      req.update
    end
    Requests.delete_if { |req| req.closed? }
  end

  class Request
    @@delay = 0
    def self.delay
      @@delay
    end

    # data 缺省时发送 GET 请求
    def initialize(url, data, method, delay, &block)
      @state = STATE_WAIT
      @count = -delay
      @method = method
      @url = url
      @data = data
      if block_given?
        @proc = block
      end
    end

    def update
      if @count < 0
        @count += 1
        return
      elsif !@query
        query_start
        return
      elsif closed?
        return
      end

      @state = query_update

      if @state != STATE_WAIT
        if @state == STATE_FINISH
          recv_data = decode(@query.data)
          @proc.call(recv_data) if @proc
        end
        @query.finalize
        @state = STATE_CLOSE
        @@delay = (@@delay + @count) / 2
        p "延迟: #{@@delay} 帧" if $output_level > 0
      end
    end

    def query_start
      case @method
      when "GET"
        query_start_get(@url, @data)
      when "POST"
        query_start_post(@url, @data)
      when "PUT"
        query_start_put(@url, @data)
      end
    end

    def query_start_post(url, data)
      query = AsHttp::Query.make(url, "POST")
      query.set_header("Content-Type", "application/json;charset=utf-8")
      post_data = encode(data)
      query.set_data(post_data)

      @query = query
      @query.start
    end

    def query_start_get(url, data)
      query = AsHttp::Query.make(url + data, "GET")
      @query = query
      @query.start
    end

    # data: name, content, post_data
    def query_start_put(url, data)
      query = AsHttp::Query.make(url, "POST")
      boundary = "-" * 30 + rand(1 << 128).to_s(36)
      query.set_header("Content-Type", "multipart/form-data;boundary=#{boundary}")

      content = data["content"]
      data.delete("content")

      key = AES.random_iv
      data["aeskey"] = Hash2.update(key, :encode)

      put_data = ""
      # -- data --
      put_data << "--" << boundary << "\r\n"
      put_data << "Content-Disposition: form-data; name=\"json\";\r\n"
      put_data << "\r\n"
      put_data << encode(data)
      put_data << "\r\n"
      # -- file --
      put_data << "--" << boundary << "\r\n"
      put_data << "Content-Disposition: form-data; name=\"file\"; filename=\"file\";\r\n"
      put_data << "Content-Type: text/plain\r\n"
      put_data << "\r\n"
      put_data << AES.encrypt(content, key)
      put_data << "\r\n--" << boundary << "--"
      query.set_data(put_data)

      @query = query
      @query.start
    end

    def query_update
      @count += 1
      if @count > RGSOS::HTTP_WAIT_FRAMES
        return STATE_ERROR
      end

      if @query.ended?
        if @query.finished?
          return STATE_FINISH
        else
          return STATE_ERROR
        end
      end
      return STATE_WAIT
    end

    def closed?
      @state == STATE_CLOSE
    end

    def encode(data)
      "r=" + RSA.encrypt_oaep(OkJson.encode(data))
    end

    def decode(data)
      if $output_level > 2
        File.open("http.log", "a") { |f|
          f.puts Time.now.to_s
          f.puts data
        }
      end
      # data.sub!(/<script>.+<\/script>$/, "") # 移除实名认证后缀
      type = data[0...2]
      data.slice!(0...2)
      case type
      when "j="
        return OkJson.decode(data)
      when "t="
        return AES.decrypt(data)
      when "a="
        return AES.decode(data)
      else
        return type + data
      end
    end
  end
end
