# encoding: UTF-8

# ==================================================
# 控制台
# --------------------------------------------------
# 调用方法 debug(binding) 开始调试
# 输入空行停止调试
# ==================================================
if $output_level > -2
  if $TEST || $DEBUG
    if !RUBY_19
      hWnd = Win32API.new("user32", "GetActiveWindow", "v", "l").call
      Win32API.new("kernel32", "AllocConsole", "", "L").call
      Win32API.new("user32", "SetForegroundWindow", "l", "l").call(hWnd)
      STDOUT.reopen "CONOUT$"
    end
    system "CHCP 65001 > NUL"
    STDIN.reopen "CONIN$"
  end

  module Kernel
    def p(*args)
      args.each do |a|
        a = a.inspect if !a.is_a? String
        a.force_encoding("UTF-8") if RUBY_19
        t = Time.now.strftime("%H:%M:%S")
        STDOUT.puts("[#{t}]: #{a}")
      end
    end

    # 用法 debug binding
    def debug(b = binding)
      STDOUT.puts "DEBUG MODE"
      loop do
        begin
          STDOUT.print(">: ")
          code = gets
          ret = eval(code, b)
        rescue Exception
          $!.inspect.split("\n").each do |line|
            STDOUT.puts("!> #{line}")
          end
          retry
        end
        if code == "\n"
          STDOUT.puts "EXIT DEBUG"
          break
        else
          begin
            out = OkJson.encode(ret)
          rescue
            out = "#{ret}"
          end
          STDOUT.puts("=: " + out)
        end
      end
    end
  end

  p "Console is ready." if $output_level > -1
  p "Debug mode：#{$TEST || $DEBUG}" if $output_level > -1
  p "Ruby version：#{RUBY_VERSION}" if $output_level > -1
end
