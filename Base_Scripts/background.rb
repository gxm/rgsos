# encoding: UTF-8

#--------------------------------------------------------------------------
# 后台运行脚本 (RGD)
#--------------------------------------------------------------------------
if defined? RGD
  p "Detected RGD..." if $output_level > -1
  p "Use RGD background running." if $output_level > -1
  Graphics.background_exec = true
else
  #------------------------------------------------------------------------
  #                                             -----SixRC      2017.11.02
  #     后台运行脚本 含失去焦点时屏蔽键盘鼠标功能
  #
  #        使用前请务必阅读此  否则脚本会失败
  #     使用为 BGR.bgr(bool) true为开启 false关闭 默认true
  #   BGR.update为在窗口失去或获得焦点时对于是否屏蔽键盘鼠标的刷新
  #   下面的注释那里把它放进了Graphics.update  请自行选择要不要这样
  #       然后
  #      因为这个脚本涉及到对具体dll的修改 不同的dll情况不同
  #   下面是不同dll对应的地址 请在 45 行开始自行修改DLL名和偏移地址及第二部分大小
  #   除DLL外需要修改三处地方 见下面 47 48 行
  #   RGSS103J                0x2E48     \x46
  #   RGSS102J 100J           0x2E09     \x46
  #   RGSS104E                0x4FA4     \x5F
  #   RGSS200J 200E 202E      0x19AA     \x35
  #   RGSS300  301            0x2712     \x5A
  #------------------------------------------------------------------------
  module BGR
    DLL_Config = {
      "RGSS103J" => [0x2E48, "\x46"],
      "RGSS102J" => [0x2E09, "\x46"], "RGSS100J" => [0x2E09, "\x46"],
      "RGSS104E" => [0x4FA4, "\x5F"],
      "RGSS200J" => [0x19AA, "\x35"], "RGSS200E" => [0x19AA, "\x35"], "RGSS202E" => [0x19AA, "\x35"],
      "RGSS300" => [0x2712, "\x5A"], "RGSS301" => [0x2712, "\x5A"],
    }

    module_function
    GetActiveWindow = Win32API.new("user32", "GetActiveWindow", "v", "l")
    Hwnd = GetActiveWindow.call
    User32 = Win32API.new("kernel32", "LoadLibraryA", "p", "l").call("user32")
    GetAsyncKeyState = Win32API.new("kernel32", "GetProcAddress", "lp", "l").call(User32, "GetAsyncKeyState")
    GetKeyState = Win32API.new("kernel32", "GetProcAddress", "lp", "l").call(User32, "GetKeyState")
    WriteProcessMemory = Win32API.new("kernel32", "WriteProcessMemory", "llpll", "l")
    Original = "\0" * 5
    OriginalVA = "\0" * 5
    Mask = "\x31\xC0\xC2\x04\x00"
    Win32API.new("kernel32", "ReadProcessMemory", "llpll", "l").call(-1, GetAsyncKeyState, Original, 5, 0)
    Win32API.new("kernel32", "ReadProcessMemory", "llpll", "l").call(-1, GetKeyState, OriginalVA, 5, 0)
    @state = 0

    def update
      if @state == 0 && Hwnd != GetActiveWindow.call
        WriteProcessMemory.call(-1, GetAsyncKeyState, Mask, 5, 0)
        WriteProcessMemory.call(-1, GetKeyState, Mask, 5, 0)
        return @state = 1
      end
      if @state == 1 && Hwnd == GetActiveWindow.call
        WriteProcessMemory.call(-1, GetAsyncKeyState, Original, 5, 0)
        WriteProcessMemory.call(-1, GetKeyState, OriginalVA, 5, 0)
        return @state = 0
      end
    end

    if File.read("Game.ini") =~ /^Library=.*(RGSS\w+)\.dll/
      DLL = Win32API.new("kernel32", "LoadLibrary", "p", "l").call($1)
      LOC, SHIFT = DLL_Config[$1]
      p "Use #{$1} dll background running." if $output_level > -1
    else
      raise
    end

    def bgr(x = true)
      WriteProcessMemory.call(-1, DLL + LOC, "\0", 1, 0) if x
      WriteProcessMemory.call(-1, DLL + LOC, SHIFT, 1, 0) unless x
    end
  end

  BGR.bgr
end
