# encoding: UTF-8

class Tutorial
  Path_MD = "../tutorial.md"
  Data = {}
  key = nil
  code = false
  File.open(Path_MD, "r") do |f|
    f.each_line do |line|
      line.chop!
      if line =~ /^```/
        code = !code
      end
      if !code && line =~ /^\#+/
        key = line.sub(/^\#+/, "").strip
        Data[key] = []
        next
      end
      if code || line =~ /^```/
        Data[key].push(line)
      else
        line.strip!
        line.gsub!(/\*\*(.+?)\*\*/, '\c[2]\1\c[0]')
        line.gsub!(/`(.+?)`/, '\c[4]\1\c[0]')
        Data[key].push(line)
      end
    end
  end

  Data.values.each do |v|
    v.pop while v.last == ""
    v.shift while v.first == ""
  end

  def initialize(key)
    @data = Data[key]
    @index = 0
    @end = false
    @print = false
  end

  def get
    for i in 1..4
      $game_variables[i] = ""
    end
    @end = false
    @index += 1 while line && line.empty?
    if !line
      @end = true
      return
    end

    if line =~ /^```/
      code = ""
      loop do
        @index += 1
        if !line || line =~ /```/
          @index += 1
          break
        else
          code << line + "\n"
        end
      end
      p "\n" + code if !code.empty?
    end

    for i in 0..3
      if !line
        @end = true
        break
      elsif line.empty?
        break
      else
        $game_variables[i + 1] = line
      end
      @index += 1
    end
  end

  def line
    @data[@index]
  end

  def end?
    @end
  end
end

class Game_Event < Game_Character
  def net_name
    @event.name
  end
end
