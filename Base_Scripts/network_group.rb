# encoding: UTF-8

# 有 4 种基本类型
class Network_Group
  # 群组，通讯，保存的数据的最小结构
  Group_Keys = {/u-\d+/ => {}}

  attr_reader :gid
  attr_reader :data
  # 在获取第一波数据之前 ready 都是 false，此时内部的 update 也不执行
  attr_accessor :ready
  attr_accessor :news

  def initialize(gid)
    @gid = gid
    @data = {}
    # 缓存数据
    # 由于数据到达可能是乱序的，所以把提前到达、过期的数据缓存起来，逐步更新
    @cache = {}
    @news = []
    @ready = false
    init_keys
  end

  def get(key)
    @data[key.to_sym]
  end

  def set(key, value)
    key = key.to_sym
    if !@cache[key]
      p "Cannot set: #{key}" if $output_level > -1
      return
    end
    case @cache[key][:kind]
    when :V
      setV(key, value, "set")
    when :S
      setS(key, value, nil)
    end
  end

  def setV(key, value, method)
    data = {"action" => "update_value", "key" => key.to_s, "value" => value, "method" => method}
    send_ws(data)
  end

  def setS(key, update_data, uid)
    data = {"action" => "update_state", "key" => key.to_s, "update" => update_data, "uid" => uid}
    data["uid"] = $net.uid if uid == nil
    send_ws(data)
  end

  def dupS
    data = {"action" => "dup_state"}
    send_ws(data)
  end

  def putF(path, content, &block)
    put_data = {"path" => path, "content" => content, "gid" => @gid, "uid" => $net.uid}
    Net_HTTP::connect($net.server_url, put_data, "PUT", &block)
  end

  def getF(path, &block)
    post_data = {"action" => "get_file", "path" => path, "gid" => @gid, "uid" => $net.uid}
    Net_HTTP::connect($net.server_url, post_data, "POST", &block)
  end

  def listF(&block)
    post_data = {"action" => "list_file", "gid" => @gid, "uid" => $net.uid}
    Net_HTTP::connect($net.server_url, post_data, "POST", &block)
  end

  # 主动发送数据
  def send_ws(data)
    $net.send_ws(@gid, data) if @ready
  end

  # 回调数据
  def recv_ws(error, data)
    if error == 0
      if data.keys.include?("t")
        if !data["keys"].empty?
          set_data(data["keys"], data["values"])
        end
      else
        data.each do |key, update_data|
          key = key.to_sym
          cache = @cache[key]
          next if !cache
          case cache[:kind]
          when :V
            method, value, t = update_data
            $net.adjust_time(t)
            case method.to_sym
            when :push
              @data[key].push(value)
            when :diff
              @data[key].delete(value)
            when :set, :get
              @data[key] = value if t > cache[:t]
            when :unset
              @data[key].delete(value) if t > cache[:t]
            end
            if t < cache[:t]
              cache[:expire] = 0
            else
              cache[:t] = t
              cache[:expire] = random_expire_time
            end
          when :S
            id, update, t = update_data
            $net.adjust_time(t)
            if id == ""
              d = @data[key]
            else
              @data[key][id] ||= {}
              d = @data[key][id]
            end
            for k, v in update
              if !cache[:t][k] || t > cache[:t][k]
                if v.nil?
                  d.delete(k)
                else
                  d[k] = v
                end
                cache[:expire] = random_expire_time
                cache[:t][k] = t
              end
            end
          end
          # -------------------------------------------------
          # update news
          # -------------------------------------------------
          @news.push(key)
          p "#{gid} | news: #{OkJson.encode @news.map { |n| n.to_s }}" if $output_level > 1
          # -------------------------------------------------
        end
      end
    else
      p "#{gid} | recv: error in ws: #{data}" if $output_level > -1
    end
  end

  # 外部调用的方法，周期性判断各个 keys 的过期情况
  def update
    @cache.each do |key, cache|
      # 设置为 nil 不再主动更新
      next if cache == nil
      # 减少过期时间
      cache[:expire] -= 1
      if cache[:expire] < 0
        p "#{gid} | `#{key}` was expired." if $output_level > 0
        case cache[:kind]
        when :V
          data = {"action" => "get_value", "key" => key.to_s}
        when :S
          data = {"action" => "get_state", "key" => key.to_s}
        else
          data = nil
        end
        send_ws(data) if data
        cache[:expire] = random_expire_time
      end
    end
  end

  # 初始数据
  # 格式是 {kind1 => [k1, k2, k3...], kind2 => ...}
  def init_keys
    p "#{@gid} | init" if $output_level > 0
    @data.clear
    for kind, keys_array in group_keys
      for k in keys_array
        # 类型 / current_index / max_index
        case kind
        when "value"
          @data[k] = []
          # kind, expire_time, time_strap
          @cache[k] = {:kind => :V, :expire => 0, :t => 0}
        when "state"
          @data[k] = {}
          @cache[k] = {:kind => :S, :expire => 0, :t => {}}
        end
      end
    end
    # 获取基础数据，等待 1s 的间隔

    post_data = {"action" => "get_data", "gid" => @gid, "uid" => $net.uid}
    p "#{gid} | keys: fetching..." if $output_level > 0
    Net_HTTP::connect($net.server_url, post_data, "POST", 60) do |error, data|
      if error == 0
        set_data(data["keys"], data["values"])
        @ready = true
        p "#{gid} | ready" if $output_level > -1
      else
        p "#{gid} | keys: failed to fetch data: #{data}" if $output_level > -1
      end
    end
  end

  def set_data(keys, values)
    keys.zip(values).each do |key, value|
      # -------------------------------------------------
      # 注意:
      # 来自 websocket 的 get_state 请求的回复，也会触发这里的赋值
      # -------------------------------------------------
      if key.include? "/" # State
        k, id = key.split("/")
        k = k.to_sym
        # next if @data[k] == nil
        @data[k] ||= {}
        if value == []
          value = {}
        end
        if id == ""
          @data[k] = value
        else
          @data[k][id] = value
        end
        @news.push(k)
      else # Value
        key = key.to_sym
        # -------------------------------------------------
        # 代码含义：
        # 进入群组时下载的 key ，可能群组里并没有定义
        # 删除原因：
        # 其实无伤大雅，此数据就不自动同步了，但是读下来没有问题
        # 上面关于 state 的也一样
        # -------------------------------------------------
        # next if @data[key] == nil
        # -------------------------------------------------
        @data[key] = value
        @news.push(key)
        cache = @cache[key]
        next if !cache
        cache[:expire] = random_expire_time
      end
    end
    p "#{gid} | setk: #{keys.join(", ")}" if $output_level > 0
    p "#{gid} | data: #{OkJson.encode @data.values}" if $output_level > 1
  end

  def refresh(*keys)
    if keys.empty?
      keys = group_keys.values.flatten
    end
    keys.each do |key|
      next if !@cache[key]
      @cache[key][:expire] = 0
    end
  end

  # 其他操作
  def group_keys
    _keys = {"value" => [], "state" => []}
    Group_Keys.each do |reg, keys|
      next if reg.is_a?(String) && @gid != reg
      next if reg.is_a?(Regexp) && @gid !~ reg
      for kind, keys_array in keys
        _keys[kind] += keys_array.collect { |k| k.to_sym }
      end
    end
    return _keys
  end

  def random_expire_time(limit = RGSOS::VALUE_EXPIRE_FRAMES)
    limit + rand(limit)
  end

  def method_missing(m, *args, &block)
    if @data.keys.include? m
      return get(m)
    end
    if m.to_s =~ /=$/
      k = m.to_s.chop.to_sym
      if @data.keys.include? k
        return set(k, args[0])
      end
    end
    super
  end
end
