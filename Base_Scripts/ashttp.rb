# encoding: UTF-8

#For AsHttp v0.3 by AzureFx
module AsHttp
  NULL = 0
  INFINITE = 0xFFFFFFFF
  DLL_PATH = "System/AsHttp.dll"
  INVALID_QUERY_VALUE = 0
  UNKNOWN = 0
  READY = 1
  SENDING = 2
  RECEIVING = 3
  FINISHED = 4
  FAILED = 5

  CreateQueryA = Win32API.new(DLL_PATH, "CreateQueryA", ["p", "p"], "i")
  SetHeaderA = Win32API.new(DLL_PATH, "SetHeaderA", ["i", "p", "p"], "i")
  SetData = Win32API.new(DLL_PATH, "SetData", ["i", "p", "i"], "i")
  EnableAutoRedirect = Win32API.new(DLL_PATH, "EnableAutoRedirect", ["i", "i"], "i")
  StartQuery = Win32API.new(DLL_PATH, "StartQuery", "i", "i")
  WaitForQuery = Win32API.new(DLL_PATH, "WaitForQuery", ["i", "i"], "i")
  GetStatusCode = Win32API.new(DLL_PATH, "GetStatusCode", ["i"], "i")
  GetRawHeader = Win32API.new(DLL_PATH, "GetRawHeader", ["i", "p", "p"], "i")
  GetHeaderA = Win32API.new(DLL_PATH, "GetHeaderA", ["i", "p", "p", "p"], "i")
  GetProgress = Win32API.new(DLL_PATH, "GetProgress", ["i", "p", "p"], "i")
  GetContent = Win32API.new(DLL_PATH, "GetContent", ["i", "p", "p"], "i")
  FinishQuery = Win32API.new(DLL_PATH, "FinishQuery", "i", "i")

  class Query
    def initialize(url, method)
      @hQuery = CreateQueryA.call(url, method)
    end

    def self.make(url, method)
      query = Query.new(url, method)
      if query.handle == INVALID_QUERY_VALUE
        return nil
      end
      return query
    end

    def handle
      @hQuery
    end

    def set_auto_redirect(enable)
      EnableAutoRedirect.call(@hQuery, enable == true ? 1 : 0)
      return self
    end

    def set_header(key, value)
      SetHeaderA.call(@hQuery, key, value)
      return self
    end

    def set_data(data)
      # SetData.call(@hQuery, data, data.length)
      SetData.call(@hQuery, data, data.bytesize)
      return self
    end

    def start(&callback)
      StartQuery.call(@hQuery)
      if callback != nil
        Thread.new {
          while true
            sleep(0.5)
            if ended?
              break
            end
          end
          callback.call self
        }
      end
      return self
    end

    def wait(dwMilliseconds = INFINITE)
      WaitForQuery.call(@hQuery, dwMilliseconds)
      return self
    end

    def status
      bytes_total = [0].pack("i")
      bytes_loaded = [0].pack("i")
      status = GetProgress.call(@hQuery, bytes_total, bytes_loaded)
      return status, bytes_total.unpack("i")[0], bytes_loaded.unpack("i")[0]
    end

    def ended?
      stat = status
      return stat[0] == FINISHED || stat[0] == FAILED
    end

    def finished?
      stat = status
      return stat[0] == FINISHED
    end

    def status_code
      return GetStatusCode.call(@hQuery)
    end

    def data
      stat = status
      if stat[0] != FINISHED
        return nil
      end
      size = [0].pack("i")
      GetContent.call(@hQuery, 0, size)
      # buffer = Array.new(size.unpack("i")[0]).pack("C*")
      buffer = "\0" * (size.unpack("i")[0])
      GetContent.call(@hQuery, buffer, size)
      return buffer
    end

    def raw_header
      header_size = [0].pack("i")
      GetRawHeader.call(@hQuery, 0, header_size)
      # buffer = Array.new(header_size.unpack("i")[0]).pack("C*")
      buffer = "\0" * (header_size.unpack("i")[0])
      GetRawHeader.call(@hQuery, buffer, header_size)
      return buffer
    end

    def header(key)
      val_size = [0].pack("i")
      GetHeaderA.call(@hQuery, key, 0, val_size)
      # buffer = Array.new(val_size.unpack("i")[0]).pack("C*")
      buffer = "\0" * (val_size.unpack("i")[0])
      GetHeaderA.call(@hQuery, key, buffer, val_size)
      return buffer
    end

    def close
      FinishQuery.call(@hQuery)
    end

    def finalize
      close
    end
  end
end
