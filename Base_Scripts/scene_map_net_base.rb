# encoding: UTF-8

# 允许多个地图在同一个 group，在这上面同步以下数据:
# 变量:
# - online: [监听]在线玩家, 更新时创建、删除（隐藏）Sprite_Character 对象
# - chat: [监听]最新的聊天信息（包括气泡），更新时调用特定的函数
# 状态:
# - player:
#   - pos: [map_id, x, y, d] 玩家的位置
#   - looks: [character_name, character_index] 玩家的形象, 未来可能添加 Followers

# 进入任意地图自动加入的群，gid 在 Game_Map#net_gid 中得到
module RGSOS
  MAP_GROUP = /_map\d+/
end

class Network_Group
  Group_Keys[RGSOS::MAP_GROUP] = {
    "value" => [:online, :chat],
    "state" => [:player],
  }
end

# Network_Base 新增了属性 map_players 表示其他的玩家
# map_players 是 Hash: {uid => Game_NetPlayer.new}
# 监听部分变量以更新 map_players
class Network_Base
  attr_reader :map_players
  attr_accessor :map_group

  alias _net_map_initialize initialize

  def initialize
    _net_map_initialize
    @map_players = {}
    @map_group = nil
    listen(RGSOS::MAP_GROUP, :online) { |g, k, v|
      if g == @map_group.gid
        self.map_online = v.uniq
      end
    }
    listen(RGSOS::MAP_GROUP, :chat) { |g, k, v|
      if g == @map_group.gid
        self.map_chat = v
      end
    }
  end

  # def map_online=(online); end
  # def map_chat=(chat); end
end

# Game_Map 中定义了 gid
# 在进入新的地图时，自动退出、加入群
class Game_Map
  def net_gid
    case @map_id
    when 1
      "_map1"
    when 2, 3
      "_map2"
    else
      "_map0#{$net.uid}"
    end
  end

  alias _net_map_setup setup

  def setup(map_id)
    _net_map_setup(map_id)
    # 退出其他的 map group
    $net.groups.keys.each { |gid|
      if gid =~ RGSOS::MAP_GROUP && gid != net_gid
        $net.exit_group(gid)
      end
    }
    gid = net_gid
    if !$net.groups.keys.include?(gid)
      $net.join_group(gid)
      $net.map_players.clear
      $net.map_group = $net.groups[gid]
    end
  end
end

# Game_Player 的任何改变都会上传到服务器进行广播
# 每次移动的时候都会立即更新位置信息
# 不移动时，周期性检查数据是否和自己收到网络信息一致
class Game_Player < Game_Character
  alias _net_map_update update

  def update
    _net_map_update
    g = $net.map_group
    if g && g.ready
      _data = net_player_data
      # pos 的立即改变
      if @_pos != _data["pos"]
        @_pos = _data["pos"]
        g.set(:player, "pos" => @_pos)
      end
      # 非移动的情况下，每 1s 强制同步
      if Graphics.frame_count % 60 == 0 && !moving?
        if g.get(:player)[$net.uid] != _data
          g.set(:player, _data)
        end
      end
    end
  end

  # def net_player_data; end
end

# Game_NetPlayer 代替其他角色的类，希望做到尽快、流畅的移动
# uid 代表对应角色的唯一编号
# 每帧调用 update 更新位置和形象
class Game_NetPlayer < Game_Character
  def initialize(uid)
    super()
    @uid = uid
    @pos = [0, 0, 0, 0] # map_id, x, y, direction
    @through = true
  end

  def update
    super
    self.player_data = $net.map_group.get(:player)[@uid] || {}
    move
  end

  # def move; end

  def in_map?
    @pos[0] == $game_map.map_id
  end

  def moveto_pos
    moveto(@pos[1], @pos[2])
  end

  def dash?
    @x != @pos[1] || @y != @pos[2]
  end

  def player_data=(data)
    data.each do |key, value|
      case key
      when "pos"
        self.pos = value
      when "looks"
        self.looks = value
      end
    end
  end

  def pos=(pos)
    @pos = pos.clone
    _switch = @transparent ^ !in_map?
    @transparent = !in_map?
    if !@transparent
      dx = @pos[1] - @x
      dy = @pos[2] - @y
      if _switch || dx * dx + dy * dy > 10
        moveto_pos
      end
    end
  end

  def looks=(looks)
    @character_name = looks[0] || ""
    @character_index = looks[1] || 0
  end
end
